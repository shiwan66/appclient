import { Component, OnInit } from '@angular/core';
import { Router, ActivatedRoute, Params } from '@angular/router';
import { Observable } from 'rxjs';
import {$WebSocket} from 'angular2-websocket/angular2-websocket'

import 'rxjs/add/operator/map';

declare var jQuery:any;
declare var CanvasBG:any;

@Component({
  selector: 'app-test',
  templateUrl: './test.component.html',
  styleUrls: ['./test.component.scss']
})
export class TestComponent implements OnInit {
//   model:any = {};
//   ws: any;
  
//   constructor(
//     private router: Router
//   ) { }

  ngOnInit() {
//     jQuery(document).ready(() => {
//       CanvasBG.init({
//           Loc: {
//               x: window.innerWidth / 2,
//               y: window.innerHeight / 2
//           }
//       });
//     })
//     this.ws = new $WebSocket("ws://localhost:3006");
//     this.ws.onMessage(
//       (msg: MessageEvent)=> {
//           console.log("onMessage ", msg.data);
//       },
//       {autoApply: false}
//   );
    
//     // set received message stream
//     this.ws.getDataStream().subscribe(
//         (msg)=> {
//             console.log("next", msg.data);
//             // this.ws.close(false);
//         },
//         (msg)=> {
//             console.log("error", msg);
//         },
//         ()=> {
//             console.log("complete");
//         }
//     );
  }

//   onSubmit() {
//     this.router.navigate(['/app/home'])
//   }




rows = [];
selected = [];

constructor() {
  this.fetch((data) => {
    this.rows = data;
  });
}

fetch(cb) {
  const req = new XMLHttpRequest();
  req.open('GET', `assets/data/company.json`);

  req.onload = () => {
    cb(JSON.parse(req.response));
  };

  req.send();
}

onSelect({ selected }) {
  console.log('Select Event', selected, this.selected);

  this.selected.splice(0, this.selected.length);
  this.selected.push(...selected);
}

onActivate(event) {
  console.log('Activate Event', event);
}

add() {
  this.selected.push(this.rows[1], this.rows[3]);
}

update() {
  this.selected = [ this.rows[1], this.rows[3] ];
}

remove() {
  this.selected = [];
}

displayCheck(row) {
  return row.name !== 'Ethel Price';
}
}
