import { NgModule } from '@angular/core';
import { SharedModule } from '../shared/shared.module'
import { TestRouterModule } from './test-router.module'


import { TestComponent } from './test/test.component';

@NgModule({
  imports: [
    SharedModule,
    TestRouterModule,
  ],
  declarations: [TestComponent]
})
export class TestModule { }
