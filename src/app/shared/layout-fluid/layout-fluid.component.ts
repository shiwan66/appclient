import { Component, OnInit } from '@angular/core';

@Component({
  selector: 'app-layout-fluid',
  templateUrl: './layout-fluid.component.html',
  styleUrls: ['./layout-fluid.component.scss']
})
export class LayoutFluidComponent implements OnInit {

  constructor() { }

  ngOnInit() {
  }

}
