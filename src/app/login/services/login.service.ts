import { Injectable } from '@angular/core';
import { Observable } from 'rxjs';
import { Http, Headers, Response, RequestOptionsArgs } from '@angular/http';
import { Router } from '@angular/router';
import 'rxjs/add/operator/map';

@Injectable()
export class LoginService {

  constructor(private http: Http, private router: Router) { }

  login(model:{username?: string, password?: string}):Observable<any> {
    let url = `/a/login`;
    let myHeader = new Headers();
    myHeader.append("Content-Type", "application/x-www-form-urlencoded");
    // var formData = new FormData();
    // formData.append("username", model.username);
    // formData.append("password", model.password);
    // let body = new URLSearchParams();
    // body.set('username', model.username);
    // body.set('password', model.password);
    let body = `username=${model.username}&password=${model.password}`
    return this.http.post(url, body, {headers: myHeader}).map(result => {
      return result;
    })
  }

}
