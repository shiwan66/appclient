import { Component, OnInit } from '@angular/core';
import { Router, ActivatedRoute, Params } from '@angular/router';
import { LoginService } from '../services/login.service'
import { AuthService } from '../../core/auth.service';

declare var jQuery:any;
declare var CanvasBG:any;

@Component({
  selector: 'app-login',
  templateUrl: './login.component.html',
  styleUrls: ['./login.component.scss']
})
export class LoginComponent implements OnInit {
  model:{username?: string, password?: string} = {};
  message: string;
  constructor(
    private router: Router,
    private loginSvc: LoginService,
    private auth: AuthService
  ) { }

  ngOnInit() {
    jQuery(document).ready(() => {
      CanvasBG.init({
          Loc: {
              x: window.innerWidth / 2,
              y: window.innerHeight / 2
          }
      });
    })
  }

  onSubmit() {
    this.loginSvc.login(this.model).subscribe(result => {
      try {
        if(result && result.json() && result.json().success) {
          this.auth.isLoggedIn = true;
          sessionStorage.setItem("isLoggedIn", JSON.stringify(this.auth.isLoggedIn));
          sessionStorage.setItem("username", this.model.username);
          this.router.navigate(['/app/home'])
        } else {
          this.message = "用户名或密码错误，请重新登陆！"
          this.model = {};
          setTimeout(() => {
            this.message= null;
          } , 15000);
        }
      } catch(ex) {
        this.message = "用户名或密码错误，请重新登陆！"
        this.model = {};
        setTimeout(() => {
          this.message= null;
        } , 15000);
      }
    })
  }

}
