import { NgModule } from '@angular/core';
import { SharedModule } from '../shared/shared.module'
import { LoginRouterModule } from './login-router.module'

import { LoginService } from './services/login.service'

import { LoginComponent } from './login/login.component';

@NgModule({
  imports: [
    SharedModule,
    LoginRouterModule
  ],
  declarations: [LoginComponent],
  providers: [LoginService]
})
export class LoginModule { }
