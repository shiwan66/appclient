import { Injectable } from '@angular/core';
import { Observable } from 'rxjs';
import { Http, Headers, Response, RequestOptionsArgs } from '@angular/http';
import { Router } from '@angular/router';
import 'rxjs/add/operator/map';
@Injectable()
export class WarningService {

    serverUrl = '/' + location.pathname.split("/")[1] + '/a/';
    constructor(private http: Http, private router: Router) { }

    //环境态势测试数据获取
    getWeekWarning(): Observable<any> {
        let url = '/a/info/alarm/weekdatas';
        return this.http.get(url).map(result => {
          var datas = result.json().datas;
          return datas.map(item => {
              return {
                name: item.dateLabel,
                value: item.alarmQuantity
              }
          });
        })
    }

    //环境态势报警数据获取
    getDeviceWarning(): Observable<any> {
        let url = '/a/info/alarm/instrumentquantities';
        return this.http.get(url).map(result => {
          return result.json().datas.map(item => {
              return {
                  name: item.instrumentName,
                  value: item.rankTenNum
              }
          });
        })
    }

    getDateWarning(): Observable<any> {
        let url = '/a/info/alarm/monthhistory';
        let username = sessionStorage.getItem("username");
        if(username) {
            url+=`?username=${username}`;
        }
        return this.http.get(url).map(result => {
            // return result.json();
            result = result.json();
            (<any>result).datas.forEach(element => {
                if(element.threshold1 == -10000) element.threshold1 = "---";
            });
            return result;
        })
    }

}
