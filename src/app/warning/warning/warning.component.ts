import { Component, OnInit } from '@angular/core';
import { WarningService } from '../services/warning.service'

@Component({
  selector: 'app-warning',
  templateUrl: './warning.component.html',
  styleUrls: ['./warning.component.scss']
})
export class WarningComponent implements OnInit {
  model:Object = {};
  data: any[];
  constructor(private warningSvc: WarningService) { }

  ngOnInit() {
    this.warningSvc.getDateWarning().subscribe(result => {
      if(result) {
        this.data = result.datas;
      }
    })
  }

}
