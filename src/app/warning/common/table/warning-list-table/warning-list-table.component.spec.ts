import { async, ComponentFixture, TestBed } from '@angular/core/testing';

import { WarningListTableComponent } from './warning-list-table.component';

describe('WarningListTableComponent', () => {
  let component: WarningListTableComponent;
  let fixture: ComponentFixture<WarningListTableComponent>;

  beforeEach(async(() => {
    TestBed.configureTestingModule({
      declarations: [ WarningListTableComponent ]
    })
    .compileComponents();
  }));

  beforeEach(() => {
    fixture = TestBed.createComponent(WarningListTableComponent);
    component = fixture.componentInstance;
    fixture.detectChanges();
  });

  it('should create', () => {
    expect(component).toBeTruthy();
  });
});
