import { Component, OnInit } from '@angular/core';
import { WarningService } from '../../../services/warning.service'

@Component({
  selector: 'app-week-warning-chart',
  templateUrl: './week-warning-chart.component.html',
  styleUrls: ['./week-warning-chart.component.scss']
})
export class WeekWarningChartComponent implements OnInit {
  data: any[];
  lineoption = {
    title: {
      text: '最近一周通知数变化'
    },
    tooltip: {
      trigger: 'axis'
    },
    legend: {
      data: ['最高通知数']
    },
    grid: {
      x: 40,
      x2: 40,
      y2: 24
    },
    calculable: true,
    xAxis: [
      {
        type: 'category',
        boundaryGap: false
      }
    ],
    yAxis: [
      {
        type: 'value',
        axisLabel: {
          formatter: '{value}条'
        }
      }
    ],
    series: [
      {
        name: '告警数',
        type: 'line',
        markPoint: {
          data: [
            { type: 'max', name: '最大值' },
            { type: 'min', name: '最小值' }
          ]
        },
        markLine: {
          data: [
            { type: 'average', name: '平均值' }
          ]
        }
      }

    ]
  };
  updateLineoption: any;
  constructor(private warningSvc: WarningService) { }

  ngOnInit() {
    var that = this;
    this.warningSvc.getWeekWarning().subscribe(result => {
      if (result) {
        var data = result.map(item => {
          return {
            name: item.name,
            value: [item.name, item.value]
          }
        })
        this.updateLineoption = {
          series: [{
            data: data
          }]
        }
      }
    })
  }
  getDay(day:string) {
    switch(day) {
      case 'mondayNum':
        return '星期一';
      case "tuesdayNum":
        return '星期二';
      case "wensdayNum":
        return "星期三";
      case "thursdayNum":
        return "星期四";
      case "fridayNum":
        return "星期五";
      case "saturdayNum":
        return "星期六";
      case "sundayNum":
        return "星期日";
      default: 
        return "other";
    }
  }

}
