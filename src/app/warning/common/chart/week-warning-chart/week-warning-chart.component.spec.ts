import { async, ComponentFixture, TestBed } from '@angular/core/testing';

import { WeekWarningChartComponent } from './week-warning-chart.component';

describe('WeekWarningChartComponent', () => {
  let component: WeekWarningChartComponent;
  let fixture: ComponentFixture<WeekWarningChartComponent>;

  beforeEach(async(() => {
    TestBed.configureTestingModule({
      declarations: [ WeekWarningChartComponent ]
    })
    .compileComponents();
  }));

  beforeEach(() => {
    fixture = TestBed.createComponent(WeekWarningChartComponent);
    component = fixture.componentInstance;
    fixture.detectChanges();
  });

  it('should create', () => {
    expect(component).toBeTruthy();
  });
});
