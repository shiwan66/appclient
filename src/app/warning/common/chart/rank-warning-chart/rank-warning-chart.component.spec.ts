import { async, ComponentFixture, TestBed } from '@angular/core/testing';

import { RankWarningChartComponent } from './rank-warning-chart.component';

describe('RankWarningChartComponent', () => {
  let component: RankWarningChartComponent;
  let fixture: ComponentFixture<RankWarningChartComponent>;

  beforeEach(async(() => {
    TestBed.configureTestingModule({
      declarations: [ RankWarningChartComponent ]
    })
    .compileComponents();
  }));

  beforeEach(() => {
    fixture = TestBed.createComponent(RankWarningChartComponent);
    component = fixture.componentInstance;
    fixture.detectChanges();
  });

  it('should create', () => {
    expect(component).toBeTruthy();
  });
});
