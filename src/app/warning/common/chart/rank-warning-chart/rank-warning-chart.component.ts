import { Component, OnInit } from '@angular/core';
import { WarningService } from '../../../services/warning.service'

@Component({
  selector: 'app-rank-warning-chart',
  templateUrl: './rank-warning-chart.component.html',
  styleUrls: ['./rank-warning-chart.component.scss']
})
export class RankWarningChartComponent implements OnInit {
  data: any[];
  baroption = {
    title: {
      text: '本周告警数'
    },
    tooltip: {
      trigger: 'axis'
    },
    legend: {
      data: ['告警数']
    },
    grid: {
      x: 30,
      x2: 40,
      y2: 24
    },
    calculable: true,
    xAxis: [
      {
        type: 'category',
        axisLabel: {
            // rotate: 12,
        },
      }
    ],
    yAxis: [
      {
        type: 'value'
      }
    ],
    series: [
      {
        name: '告警数',
        type: 'bar',
        data: this.data,
        markPoint: {
          data: [
            { type: 'max', name: '最大值' },
            { type: 'min', name: '最小值' }
          ]
        },
        markLine: {
          data: [
            { type: 'average', name: '平均值' }
          ]
        }
      }
    ]
  };
  updateBaroption: any;
  constructor(private warningSvc: WarningService) { }

  ngOnInit() {
    this.warningSvc.getDeviceWarning().subscribe(result => {
      if(result) {
        this.updateBaroption = {
          series:[{
            data: result.map(item=> {
              return {
                name: item.name,
                value: [item.name.substr(0,3)+"..."+item.name.substr(-3, 3), item.value]
              }
            })
          }]
        }
      }
    })
  }

}
