import { NgModule } from '@angular/core';
import { SharedModule } from '../shared/shared.module'
import { WarningRouterModule } from './warning-router.module'
import { WarningService } from './services/warning.service'


import { WarningComponent } from './warning/warning.component';
import { WeekWarningChartComponent } from './common/chart/week-warning-chart/week-warning-chart.component';
import { RankWarningChartComponent } from './common/chart/rank-warning-chart/rank-warning-chart.component';
import { WarningListTableComponent } from './common/table/warning-list-table/warning-list-table.component';

@NgModule({
  imports: [
    SharedModule,
    WarningRouterModule
  ],
  declarations: [WarningComponent, WeekWarningChartComponent, RankWarningChartComponent, WarningListTableComponent],
  providers: [WarningService]
})
export class WarningModule { }
