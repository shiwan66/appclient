import { NgModule } from '@angular/core';
import { RouterModule, Routes } from "@angular/router";
import { AuthGuardService } from '../core/auth-guard.service'
import { WarningComponent } from './warning/warning.component';

const routes: Routes = [
  {path: '', canActivate:[AuthGuardService], component: WarningComponent}
]

@NgModule({
  imports: [
    RouterModule.forChild(routes)
  ],
  exports: [RouterModule]
})
export class WarningRouterModule { }
