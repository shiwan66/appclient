import { Injectable } from '@angular/core';
import { Observable } from 'rxjs';
import { Http, Headers, Response, RequestOptionsArgs } from '@angular/http';
import { Router } from '@angular/router';
import 'rxjs/add/operator/map';

@Injectable()
export class SysStateService {

  serverUrl = '/' + location.pathname.split("/")[1] + '/a/';
  constructor(private http: Http, private router: Router) { }

  getSysState(): Observable<any> {
    let url = `/a/sysState/get`
    return this.http.get(url).map(result => {
        return result.json();
    })
//     return Observable.of<any>({
//       "totalNumToday": 3778720731,
//       "totalNumMonth": [
//           377526398701,
//           377526398702,
//           377526398705,
//           377526398710,
//           377526398717,
//           377526398726,
//           377526398740,
//           377526398762,
//           377526398792,
//           377526398830,
//           377526398890,
//           377526398999
//       ],
//       "emq": {
//           "cpu": 1.02,
//           "mem": 67.2,
//           "time": 2772,
//           "device": 28,
//           "receive": 152367,
//           "process": 107621,
//           "topic": 204679
//       },
//       "mysql": {
//           "process": 118,
//           "storage": 7284,
//           "cpu": 63,
//           "mem": 49,
//           "time": 2772,
//           "connector": 2,
//           "buffer": 30
//       },
//       "com": {
//           "cpu": 7,
//           "mem": 41,
//           "time": 2772,
//           "receive": 273118,
//           "upload": 2745425,
//           "buffer": 24988,
//           "bytes": 3289596
//       },
//       "app": {
//           "cpu": 35,
//           "mem": 23,
//           "time": 2772,
//           "connect": 274053,
//           "receive": 275432,
//           "upload": 285571,
//           "bytes": 3322650
//       },
//       "mvc": {
//           "answerTime": 339,
//           "cpu": 50,
//           "mem": 20,
//           "time": 2772,
//           "client": 1036,
//           "connects": 513,
//           "bytes": 3264059
//       }
//   })
  }
  

}
