import { TestBed, inject } from '@angular/core/testing';

import { SysStateService } from './sys-state.service';

describe('SysStateService', () => {
  beforeEach(() => {
    TestBed.configureTestingModule({
      providers: [SysStateService]
    });
  });

  it('should be created', inject([SysStateService], (service: SysStateService) => {
    expect(service).toBeTruthy();
  }));
});
