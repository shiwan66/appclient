import { async, ComponentFixture, TestBed } from '@angular/core/testing';

import { SysStateComponent } from './sys-state.component';

describe('SysStateComponent', () => {
  let component: SysStateComponent;
  let fixture: ComponentFixture<SysStateComponent>;

  beforeEach(async(() => {
    TestBed.configureTestingModule({
      declarations: [ SysStateComponent ]
    })
    .compileComponents();
  }));

  beforeEach(() => {
    fixture = TestBed.createComponent(SysStateComponent);
    component = fixture.componentInstance;
    fixture.detectChanges();
  });

  it('should create', () => {
    expect(component).toBeTruthy();
  });
});
