import { Component, OnInit, ViewChild, TemplateRef } from '@angular/core';
import { SysStateService } from '../services/sys-state.service'

@Component({
  selector: 'app-sys-state',
  templateUrl: './sys-state.component.html',
  styleUrls: ['./sys-state.component.scss']
})
export class SysStateComponent implements OnInit {
  total: number = 5432100;
  totalStr: string = "";
  totalStrList: number[] = [];
  strTimer: any;
  public emq: any[];
  public mysql: any[];
  public com: any[];
  public app: any[];
  public mvc: any[];
  public emqStting: any = {title: '物联网服务器', horizontal: true};
  public mysqlStting: any = {title: '数据库服务器', horizontal: true};
  public comStting: any = {title: '通讯转发器', horizontal: false};
  public appStting: any = {title: '云端处理器', horizontal: false};
  public mvcStting: any = {title: '应用服务器', horizontal: false};

	//根据宽度显示state chart
	isLaptop:boolean = false;
	@ViewChild('wrap') 
  wrap: TemplateRef<any>;
  
  private timer: any;
  constructor(private sysSvc: SysStateService) { }

  ngOnInit() {
		// 笔记本下初始化页面 
    this.isLaptop = (<any>this.wrap).nativeElement.clientWidth < 1800;
    if(this.isLaptop) {
      this.emqStting.horizontal = false;
      this.mysqlStting.horizontal = false;
    }
    this.strTimer = setInterval(this.changeTotal.bind(this), 1000);
    this.getData();
    this.timer = setInterval(() => {
      this.getData();
    }, 2000)
  }

  ngOnDestroy() {
    clearInterval(this.timer);
  }

  getData() {
      this.sysSvc.getSysState().subscribe(result => {
        if(result) {
          this.parseObj(result);
        }
      })          
  }

  parseObj(obj: any) {
    this.parseEmq(obj.emq);
    this.parseMysql(obj.mysql);
    this.parseCom(obj.com);
    this.parseMvc(obj.mvc);
    this.parseApp(obj.app);
  }

  parseEmq(emq) {
    this.emq = [];
    this.emq.push({ key: 'CPU使用率：', value: emq.cpu, unit: '%' });
    this.emq.push({ key: '内存使用率：', value: emq.mem, unit: '%' });
    this.emq.push({ key: '仪器接入量：', value: emq.time, unit: '台' });
    this.emq.push({ key: '接收消息量：', value: emq.device, unit: '条' });
    this.emq.push({ key: '发送数据量：', value: emq.receive, unit: '条' });
    this.emq.push({ key: '订阅消息量：', value: emq.process, unit: '条' });
    this.emq.push({ key: '上线工作时间：', value: emq.topic, unit: '小时' });
  }
  parseMysql(mysql) {
    this.mysql = [];
    this.mysql.push({ key: 'CPU使用率：', value: mysql.cpu, unit: '%' });
    this.mysql.push({ key: '内存使用率：', value: mysql.mem, unit: '%' });
    this.mysql.push({ key: '连接使用率：', value: mysql.connector, unit: '%' });
    this.mysql.push({ key: '缓冲池利用率：', value: mysql.buffer, unit: '%' });
    this.mysql.push({ key: '指令吞吐量：', value: mysql.process, unit: '条' });
    this.mysql.push({ key: '数据存储量：', value: mysql.storage, unit: 'GB' });
    this.mysql.push({ key: '上线工作时间：', value: mysql.time, unit: '小时' });
  }
  parseCom(com) {
    this.com = [];
    this.com.push({ key: 'CPU使用率：', value: com.cpu, unit: '%' });
    this.com.push({ key: '内存使用率：', value: com.mem, unit: '%' });
    this.com.push({ key: '接收消息数：', value: com.receive, unit: '条' });
    this.com.push({ key: '转发消息数：', value: com.upload, unit: '条' });
    this.com.push({ key: '缓冲池长度：', value: com.buffer, unit: '条' });
    this.com.push({ key: '累计处理量：', value: com.bytes, unit: 'MB' });
    this.com.push({ key: '上线工作时间：', value: com.time, unit: '小时' });
  }
  parseMvc(mvc) {
    this.mvc = [];
    this.mvc.push({ key: 'CPU使用率：', value: mvc.cpu, unit: '%' });
    this.mvc.push({ key: '内存使用率：', value: mvc.mem, unit: '%' });
    this.mvc.push({ key: '客户端数量：', value: mvc.client, unit: '个' });
    this.mvc.push({ key: '每秒连接量：', value: mvc.connects, unit: '次' });
    this.mvc.push({ key: '响应时间：', value: mvc.answerTime, unit: '毫秒' });
    this.mvc.push({ key: '累计处理量：', value: mvc.bytes, unit: 'MB' });
    this.mvc.push({ key: '上线工作时间：', value: mvc.time, unit: '小时' });
  }
  parseApp(app) {
    this.app = [];
    this.app.push({ key: 'CPU使用率：', value: app.cpu, unit: '%' });
    this.app.push({ key: '内存使用率：', value: app.mem, unit: '%' });
    this.app.push({ key: '应用接入量：', value: app.upload, unit: '个' });
    this.app.push({ key: '接收消息量：', value: app.receive, unit: '条' });
    this.app.push({ key: '发布消息量：', value: app.connect, unit: '条' });
    this.app.push({ key: '累计处理量：', value: app.bytes, unit: 'MB' });
    this.app.push({ key: '上线工作时间：', value: app.time, unit: '小时' });
  }

  changeTotal() {
    var that = this;
    this.total += Math.ceil(Math.random() * 30);
    // console.log(this.total);
    this.formatTotalStr();
  }

  formatTotalStr() {
    let str = this.total.toString()
    let length = str.length;
    let level = Math.ceil(length/3);
    let arr = [];
    for(let i = 1; i <= level; i++) {
      if(i < level) {
        arr.push(str.substr(-3*i, 3))
      } else {
        arr.push(str.substr(-3*i, length%3))
      }
    } 
    arr.reverse();
    // this.totalStr = arr.join(",");
    this.totalStrList = arr;
  }

}
