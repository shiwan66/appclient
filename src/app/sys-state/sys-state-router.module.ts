import { NgModule } from '@angular/core';
import { RouterModule, Routes } from "@angular/router";
import { AuthGuardService } from '../core/auth-guard.service'
import { SysStateComponent } from './sys-state/sys-state.component';

const routes: Routes = [
  {path: '', canActivate:[AuthGuardService], component: SysStateComponent}
]

@NgModule({
  imports: [
    RouterModule.forChild(routes)
  ],
  exports: [RouterModule]
})
export class SysStateRouterModule { }
