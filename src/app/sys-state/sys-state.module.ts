import { NgModule } from '@angular/core';
import { SharedModule } from '../shared/shared.module'
import { SysStateRouterModule } from './sys-state-router.module'
import { SysStateService } from './services/sys-state.service'

import { SysStateComponent } from './sys-state/sys-state.component';
import { SysChartComponent } from './common/chart/sys-chart/sys-chart.component';
import { InstruGrowChartComponent } from './common/chart/instru-grow-chart/instru-grow-chart.component';

@NgModule({
  imports: [
    SharedModule,
    SysStateRouterModule
  ],
  declarations: [SysStateComponent, SysChartComponent, InstruGrowChartComponent],
  providers: [SysStateService]
})
export class SysStateModule { }
