import { async, ComponentFixture, TestBed } from '@angular/core/testing';

import { InstruGrowChartComponent } from './instru-grow-chart.component';

describe('InstruGrowChartComponent', () => {
  let component: InstruGrowChartComponent;
  let fixture: ComponentFixture<InstruGrowChartComponent>;

  beforeEach(async(() => {
    TestBed.configureTestingModule({
      declarations: [ InstruGrowChartComponent ]
    })
    .compileComponents();
  }));

  beforeEach(() => {
    fixture = TestBed.createComponent(InstruGrowChartComponent);
    component = fixture.componentInstance;
    fixture.detectChanges();
  });

  it('should create', () => {
    expect(component).toBeTruthy();
  });
});
