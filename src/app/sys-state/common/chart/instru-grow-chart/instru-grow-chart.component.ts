import { Component, OnInit, Input, SimpleChanges } from '@angular/core';

@Component({
    selector: 'app-instru-grow-chart',
    templateUrl: './instru-grow-chart.component.html',
    styleUrls: ['./instru-grow-chart.component.scss']
})
export class InstruGrowChartComponent implements OnInit {
    formatOutput1X() {
        var myTime = new Date();
        switch (myTime.getMonth()) {
            case 0:
                this.output1x = ['2月', '3月', '4月', '5月', '6月', '7月', '8月', '9月', '10月', '11月', '12月', '本月'];
                break;
            case 1:
                this.output1x = ['3月', '4月', '5月', '6月', '7月', '8月', '9月', '10月', '11月', '12月', '1月', '本月'];
                break;
            case 2:
                this.output1x = ['4月', '5月', '6月', '7月', '8月', '9月', '10月', '11月', '12月', '1月', '2月', '本月'];
                break;
            case 3:
                this.output1x = ['5月', '6月', '7月', '8月', '9月', '10月', '11月', '12月', '1月', '2月', '3月', '本月'];
                break;
            case 4:
                this.output1x = ['6月', '7月', '8月', '9月', '10月', '11月', '12月', '1月', '2月', '3月', '4月', '本月'];
                break;
            case 5:
                this.output1x = ['7月', '8月', '9月', '10月', '11月', '12月', '1月', '2月', '3月', '4月', '5月', '本月'];
                break;
            case 6:
                this.output1x = ['8月', '9月', '10月', '11月', '12月', '1月', '2月', '3月', '4月', '5月', '6月', '本月'];
                break;
            case 7:
                this.output1x = ['9月', '10月', '11月', '12月', '1月', '2月', '3月', '4月', '5月', '6月', '7月', '本月'];
                break;
            case 8:
                this.output1x = ['10月', '11月', '12月', '1月', '2月', '3月', '4月', '5月', '6月', '7月', '8月', '本月'];
                break;
            case 9:
                this.output1x = ['11月', '12月', '1月', '2月', '3月', '4月', '5月', '6月', '7月', '8月', '9月', '本月'];
                break;
            case 10:
                this.output1x = ['12月', '1月', '2月', '3月', '4月', '5月', '6月', '7月', '8月', '9月', '10月', '本月'];
                break;
            case 11:
                ['1月', '2月', '3月', '4月', '5月', '6月', '7月', '8月', '9月', '10月', '11月', '本月'];
                break;
            default:
                this.output1x = ['1月', '2月', '3月', '4月', '5月', '6月', '7月', '8月', '9月', '10月', '11月', '12月'];
        }
        return this.output1x
    }
    output1x = []
    outputSeries = [];
    lineoption = {
        tooltip: {
            trigger: 'axis',
            axisPointer: {
                type: 'cross',
                label: {
                    backgroundColor: '#6a7985'
                }
            }
        },
        // backgroundColor:'', //设置无背景色
        // backgroundColor: 'rgba(0,0,0,0S)',
        grid: {//直角坐标系控制
            show: false,
            x: 30,//grid 组件离容器左侧的距离
            y: 5,
            x2: 15,
            y2: 30,
            borderWidth: 6,
            borderColor: '#ff7bac'
        },
        xAxis:
            {
                type: 'category',
                splitNumber: 10,
                splitLine: {
                    show: false,
                    onGap: false,
                    lineStyle: {
                        type: 'solid',
                        // opacity: "0.8",
                        color: '#d94746',
                        width: 2 //这里是为了突出显示加上的
                    }
                },//网格线
                boundaryGap: false,
                splitArea: { show: false },
                axisLine: {
                    lineStyle: {
                        color: '#53c6f8',
                        width: 2 //这里是为了突出显示加上的
                    }
                },
                data: this.formatOutput1X()
                // ['1月', '2月', '3月', '4月', '5月', '6月', '7月', '8月', '9月', '10月', '11月', '12月']
            },
        yAxis: {
            type: 'value',
            // axisLabel:{show:false},
            // axisTick:{
            //     show:false
            // },
            scale: true,
            axisLine: {
                lineStyle: {
                    color: '#53c6f8',
                    width: 2//这里是为了突出显示加上的
                }
            },

            //不显示网格线
            splitLine: {
                show: false
            },
            boundaryGap: false,
            splitArea: { show: false }
        },
        series: [
            {

                name: '处理数据',
                type: 'line',
                stack: 'counts',
                symbol: 'circle',
                // symbol: 'image://../asset/ico/favicon.png',     // 系列级个性化拐点图形
                symbolSize: 14,
                color: '#faa919',
                label: {
                    normal: {
                        show: false,
                        position: 'top',
                        color: '#fff'
                    }
                },
                data: (function () {
                    var res = [];
                    var len = 11;
                    //产生一个基准数据，后面的数据都依次增加一个随机值。
                    var tmp;
                    tmp = Math.round(Math.random() * 1000);
                    res.push(tmp);
                    while (len--) {
                        tmp = tmp + Math.round(Math.random() * 100);
                        res.push(tmp);
                    }
                    return res;
                })()
            }
        ]
    };
    constructor() { 
        this.formatOutput1X();
    }

    ngOnInit() {
    }

    ngOnChanges(changes: SimpleChanges) {
    }

}
