import { Component, OnInit, Input, SimpleChanges } from '@angular/core';
declare var $: any;

@Component({
  selector: 'app-sys-chart',
  templateUrl: './sys-chart.component.html',
  styleUrls: ['./sys-chart.component.scss']
})
export class SysChartComponent implements OnInit {
  @Input() index?: number = 1;
  @Input() data?: any[] = [];
  @Input() setting?: any = {};
  xData: any[] = [];
  lineoption = {
    color: ['#9bca62', '#f15a25', '#ff7bac'],
    calculable: false,
    smooth: true,
    title: {
      textStyle: { fontSize: 14, color: '#fff', verticalAlign: 'middle', align: 'center' },
      top: 'bottom',
      left: 'center'
    },
    grid: {//直角坐标系控制
      show: true,
      x: 25,//grid 组件离容器左侧的距离
      y: 26,
      x2: 25,
      y2: 26,
      borderWidth: 2,
      borderColor: '#53c6f8',
      backgroundColor: 'rgba(255,255, 255, 0.05)'
    },
    legend: {
      x: 'left',
      y: "top",
      left: 20,
      icon: 'rect',
      itemWidth: 10,
      itemHeight: 10,
      itemGap: 10,

      data: [
        { name: 'CPU', textStyle: { fontSize: 10, color: "#9bca62" } },
        { name: '内存', textStyle: { fontSize: 10, color: "#f15a25" } },
      ]
    },
    xAxis: {
      type: 'category',
      boundaryGap: false,
      data: []
    },
    yAxis: {
      type: 'value',
      axisLabel: {
        formatter: '{value}',
        textStyle: { color: '#fff' }
      },
      splitLine: {
        show: false
      },
      min: 0,
      max: 100
    },
    series: [
      {
        name: 'CPU',
        type: 'line',
        hoverAnimation: false,
        // stack: 'counts', //后一个系列的值会在前一个系列的值上相加。囧~
        color: '#028c5a',
        smooth: true,
        label: {
          normal: {
            show: true,
            position: 'top',
            color: '#fff'
          }
        },
        markLine: {                      //开始标预警线
          data: [
            { name: '标线1起点', value: 90, xAxis: -1, yAxis: 80 },      // 当xAxis为类目轴时，数值1会被理解为类目轴的index，通过xAxis:-1|MAXNUMBER可以让线到达grid边缘
            { name: '标线1终点', xAxis: 9, yAxis: 80 },             // 当xAxis为类目轴时，字符串'周三'会被理解为与类目轴的文本进行匹配
          ],
        }
      },
      {
        name: '内存',
        type: 'line',
        hoverAnimation: false,
        // stack: 'counts',
        color: '#fe3f5f',
        smooth: true,
        label: {
          normal: {
            show: true,
            position: 'top',
            color: '#fff'
          }
        },
        markLine: {                      //开始标预警线
          data: [
            { name: '标线1起点', value: 90, xAxis: -1, yAxis: 80 },      // 当xAxis为类目轴时，数值1会被理解为类目轴的index，通过xAxis:-1|MAXNUMBER可以让线到达grid边缘
            { name: '标线1终点', xAxis: 9, yAxis: 80 },             // 当xAxis为类目轴时，字符串'周三'会被理解为与类目轴的文本进行匹配
          ],
        }
      }
    ]
  };
  updateLineoption: any;
  cpus: any[];
  emes: any[]
  constructor() { }

  ngOnInit() {
    let date = new Date();
    this.xData = [`${date.getMinutes()}:${date.getSeconds()}`]
    this.cpus = [this.data[0].value];
    this.emes = [this.data[1].value];
    this.updateLineoption = {
      xAxis: {
        data: this.xData
      },
      series: [
        {
          data: this.cpus
        },
        {
          data: this.emes
        }
      ]
    }
    setInterval(() => {
      $('.emqLamp1').animate({ opacity: 'toggle' }, 1000);
    }, 2000)
  }

  ngOnChanges(changes: SimpleChanges) {
    if (this.cpus) {
      if (this.cpus.length >= 5) {
        this.cpus.shift();
        this.emes.shift();
        this.xData.shift();
      }
      let date = new Date();
      this.xData.push(`${date.getMinutes()}:${date.getSeconds()}`)
      this.cpus.push(this.data[0].value)
      this.emes.push(this.data[1].value)
      this.updateLineoption = {
        xAxis: {
          data: this.xData
        },
        series: [
          {
            data: this.cpus
          },
          {
            data: this.emes
          }
        ]
      }
    }
  }

}
