import { async, ComponentFixture, TestBed } from '@angular/core/testing';

import { SysChartComponent } from './sys-chart.component';

describe('SysChartComponent', () => {
  let component: SysChartComponent;
  let fixture: ComponentFixture<SysChartComponent>;

  beforeEach(async(() => {
    TestBed.configureTestingModule({
      declarations: [ SysChartComponent ]
    })
    .compileComponents();
  }));

  beforeEach(() => {
    fixture = TestBed.createComponent(SysChartComponent);
    component = fixture.componentInstance;
    fixture.detectChanges();
  });

  it('should create', () => {
    expect(component).toBeTruthy();
  });
});
