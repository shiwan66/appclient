export interface IInstrument {
  deviceId?: string;   //仪器id
  proNumber?: string;      //出厂编号
  type?: string;            //仪器类型
  name?: string;            //仪器名称
  proUnitId?: string;     //生产者单位ID
  purUnitId?: string;     //购买者单位ID
  proDate?: string;          //生产日期
  purDate?: string;          //购买日期
  identify?: string;        //仪器唯一标识符
  tdCode?: string;          //仪器二维码
  guaPeriod?: string;      //质保期（单位：月）
  remark1?: string;         //备注1
  remark2?: string;         //备注2
  lookpointId?: string;     //监测点Id
  lookpointName?: string;     //监测点名称
}

export interface IDeviceTypeInfo {
  typeName?: string;//仪器类型名
  alias?: string;//测量结果类型
  measureType?: string;//测量类型
  remark?: string;//测量结果类型
  alertInfoJson?: string;//	缺省告警项Json串
}

export interface IDeviceByUnit {
  unitId?: string;//单位ID
  name?: string;//单位名称
  alias?: string;//单位别名
}

export interface IInstrumentEdit {
  deviceId?: string;//仪器Id
  name?: string;//	仪器名称
  proNumber?: string;//出厂编号
  type?: string;//仪器类型
  identify?: string;//	标识符
  tdCode?: string;//	二维码
  proUnitId?: string;//	生产者单位Id
  purUnitId?: string;//	购买者单位Id
  proDate?: string|Date;//	生产日期
  purDate?: string|Date;//	购买日期
  guaPeriod?: string;//	保质期，单位：月
  remark1?: string;//	备注1
  remark2?: string;//	备注2
}