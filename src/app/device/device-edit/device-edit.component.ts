import { Component, OnInit } from '@angular/core';
import { ActivatedRoute, Params, Router } from '@angular/router';
import { AuthService } from '../../core/auth.service';
import { DeviceService } from '../services/device.service'
import { IInstrumentEdit } from '../model/IInstrument'
import { Observable } from 'rxjs';
import 'rxjs/add/operator/switchMap';

@Component({
  selector: 'app-device-edit',
  templateUrl: './device-edit.component.html',
  styleUrls: ['./device-edit.component.scss']
})
export class DeviceEditComponent implements OnInit {
  model: IInstrumentEdit = {};
  typelists: Array<{ title: string, value: string }> = [];
  units: Array<{ title: string, value: string }> = [];
  statusType: string = "Add";
  btnSubmitText: string = "确认添加";
  deviceId: string;
  isView: boolean = false;
  alertModel: { type: string, message: string };

  constructor(public auth: AuthService,
    private deviceSvc: DeviceService,
    private route: ActivatedRoute,
    private router: Router) { }

  ngOnInit() {
    this.deviceSvc.getDeviceTypeInfo().subscribe(result => {
      this.typelists = result.datas.map(item => {
        return {
          title: item.typeName,
          value: item.typeName
        }
      })
    })
    this.deviceSvc.getDeviceByUnitId().subscribe(result => {
      this.units = result.datas.map(item => {
        return {
          title: item.name,
          value: item.unitId
        }
      })
    })
    this.route.params
      .switchMap((params: Params) => {
        if (params['id']) {
          this.deviceId = params['id']
          this.isView = false;
          this.statusType = "Edit";
          return this.deviceSvc.getInstrument(this.deviceId);
        }
        return Observable.of<any>(null);
      }).subscribe(
        result => {
          if (result) {
            this.model = result as IInstrumentEdit;
            this.statusType = "Edit";
            this.btnSubmitText = "保存";
          }
        }
      );
  }
  returnPrev() {
    history.back();
  }
  onSubmit() {
    this.formatTime();
    if (this.statusType == 'Add') {
      this.deviceSvc.addInstrument(this.model).subscribe(result => {
        if (result.status) {
          this.alertModel = { type: "success", message: "操作成功." };
          setTimeout(() => this.router.navigate(['/app/device']), 1500);
        } else {
          this.alertModel = { type: 'danger', message: result.message }
          setTimeout(() => this.alertModel = null, 1500);
        }
      })
    } else if (this.statusType == 'Edit') {
      this.deviceSvc.updateInstrument(this.model).subscribe(result => {
        if (result.status) {
          this.alertModel = { type: "success", message: "操作成功." };
          setTimeout(() => this.router.navigate(['/app/device']), 1500);
        } else {
          this.alertModel = { type: 'danger', message: result.message }
          setTimeout(() => this.alertModel = null, 1500);
        }
      })
    }
  }
  formatTime() {
    if(this.model.proDate && this.model.proDate instanceof Date) {
      this.model.proDate = this.auth.dateFormat("yyyy-MM-dd hh:mm:ss", this.model.proDate);
    }
    if(this.model.purDate && this.model.purDate instanceof Date) {
      this.model.purDate = this.auth.dateFormat("yyyy-MM-dd hh:mm:ss", this.model.purDate);
    }
  }

}

