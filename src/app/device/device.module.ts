import { NgModule } from '@angular/core';
import { SharedModule } from '../shared/shared.module';
import { DeviceRouterModule } from './device-router.module'
import { DeviceService } from './services/device.service'

import { DeviceComponent } from './device/device.component';
import { DeviceEditComponent } from './device-edit/device-edit.component';
import { DeviceListTableComponent } from './common/device-list-table/device-list-table.component';
import { ImportDeviceComponent } from './common/import-device/import-device.component';

@NgModule({
  imports: [
    SharedModule,
    DeviceRouterModule
  ],
  declarations: [DeviceComponent, DeviceEditComponent, DeviceListTableComponent, ImportDeviceComponent],
  providers: [DeviceService]
})
export class DeviceModule { }
