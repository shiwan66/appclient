import { NgModule } from '@angular/core';
import { RouterModule, Routes } from "@angular/router";
import { AuthGuardService } from '../core/auth-guard.service'

import { DeviceComponent } from './device/device.component';
import { DeviceEditComponent } from './device-edit/device-edit.component';
import { ImportDeviceComponent } from './common/import-device/import-device.component';

const routes: Routes = [
  {path: '', canActivate:[AuthGuardService], component: DeviceComponent},
  {path: 'add', canActivate:[AuthGuardService], component: DeviceEditComponent},
  {path: 'edit/:id', canActivate:[AuthGuardService], component: DeviceEditComponent},
  {path: 'import', canActivate:[AuthGuardService], component: ImportDeviceComponent}
]
@NgModule({
  imports: [
    RouterModule.forChild(routes)
  ],
  exports: [RouterModule]
})
export class DeviceRouterModule { }
