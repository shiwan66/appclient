import { Injectable } from '@angular/core';
import { Observable } from 'rxjs';
import { Http, Headers, Response, RequestOptionsArgs } from '@angular/http';
import { Router } from '@angular/router';
import 'rxjs/add/operator/map';

import { IInstrument, IDeviceTypeInfo, IDeviceByUnit, IInstrumentEdit } from '../model/IInstrument'
export * from '../model/IInstrument';

@Injectable()
export class DeviceService {

  constructor(private http: Http, private router: Router) { }

  // 获取仪器列表
  getInstruments(query: string, page: number, pageSize: number): Observable<{count: number, datas:IInstrument[]}> {
    let url = `/a/device/getDeviceInfo?page=${page}&pageSize=${pageSize}&query=${query}`;
    return this.http.get(url).map(result => {
      var data = result.json();
      data.datas.forEach(element => {
        element.edit = `<a href="javascript:void(0);" style="height:0px;line-height:20px;">编辑</a>`
      });
      return data;
    })
  }

  getInstrument(id: string): Observable<IInstrumentEdit> {
    let url = `/a/device/getDeviceByInstrumentId?deviceId=${id}`;
    return this.http.get(url).map(result => {
      return result.json().datas[0];
    })
  }

  getDeviceTypeInfo(): Observable<{datas:IDeviceTypeInfo[]}> {
    let url = `/a/device/type/getDeviceTypeInfo`;
    return this.http.get(url).map(result => {
      return result.json();
    })
  }

  getDeviceByUnitId(): Observable<{datas:IDeviceByUnit[]}> {
    let url = `/a/device/getDeviceByUnitId`;
    return this.http.get(url).map(result => {
      return result.json();
    })
  }

  addInstrument(model: IInstrumentEdit): Observable<{status: boolean, message: string}> {
    const headers = new Headers({ 'Content-Type': 'application/json' });
    let url = `/a/device/addDeviceInfo`
    return this.http.post(url, JSON.stringify(model), {headers:headers}).map(result => {
      return result.json();
    })
  }

  updateInstrument(model: IInstrumentEdit): Observable<{status: boolean, message: string}> {
    const headers = new Headers({ 'Content-Type': 'application/json' });
    let url = `/a/device/updateDeviceByDeviceId`
    return this.http.put(url, JSON.stringify(model), {headers:headers}).map(result => {
      return result.json();
    })
  }

}
