import { Component, OnInit, Input, Output, EventEmitter } from '@angular/core';
import { Router } from '@angular/router'
import { DeviceService } from '../../services/device.service'
declare var $: any;
import { AuthService } from '../../../core/auth.service';
import { IInstrument } from '../../model/IInstrument'

@Component({
  selector: 'app-device-list-table',
  templateUrl: './device-list-table.component.html',
  styleUrls: ['./device-list-table.component.scss']
})
export class DeviceListTableComponent implements OnInit {
  public columns:Array<any> = [
    {title: '仪器ID',      name: 'deviceId'},
    {title: '出厂编号',    name: 'proNumber'},
    {title: '仪器类型',    name: 'type' },
    {title: '仪器名称',    name: 'name' },
    {title: '生产者单位',    name: 'proUnitName' },
    {title: '购买者单位',    name: 'purUnitName' },
    {title: '生产日期',    name: 'proDate' },
    {title: '购买日期',    name: 'purDate' },
    {title: '唯一标识',    name: 'identify' },
    {title: '二维码',    name: 'tdCode' },
    {title: '质保期(月)',    name: 'guaPeriod' },
    {title: '备注1',    name: 'remark1' },
    {title: '备注2',    name: 'remark2' },
    // {title: '监测点Id',    name: 'lookpointId' },
    // {title: '监测点名称',    name: 'lookpointName' },
    {title: '操作', name: 'edit'}
  ];
  public config = {
    className: ['table-striped', 'table-bordered', "font12"]
  }
  pageSize:number = 10;
  data: Array<any>=[];
  count: number = 0;
  public constructor(private deviceSvc: DeviceService, private auth: AuthService, private router: Router) {
  }

  public ngOnInit():void {
    this.refresh();
  }

  cellClick(data) {
    var result = data.row[data.column];
    if(data.column == 'edit') {
      this.router.navigate(['/app/device/edit', data.row.deviceId])
    }
  }

	refresh() {
    this.data = null;
    this.deviceSvc.getInstruments("", 1, this.pageSize).subscribe(result => {
      if(result) {
        this.data = result.datas;
        this.count = result.count;
      }
    }, (err) => {
        this.auth.handleError(err);
      }
    )
  }
  
  pagingChange(result) {
    this.deviceSvc.getInstruments("", result.page, result.pageSize).subscribe(result => {
      if(result) {
        this.data = result.datas;
        this.count = result.count;
      }
    }, (err) => {
      this.auth.handleError(err);
    });
  }
}