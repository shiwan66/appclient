import { Component, OnInit } from '@angular/core';
import { ActivatedRoute, Params, Router } from '@angular/router';

@Component({
  selector: 'app-import-device',
  templateUrl: './import-device.component.html',
  styleUrls: ['./import-device.component.scss']
})
export class ImportDeviceComponent implements OnInit {
  type: string = 'ImportDvice';
  title: string = '仪器';
	fileName: string = '';
  // model: ImportEditModel = {};
  model: any;
	progress: number = null;
	isFile: boolean = false;
	// public alertModel: AlertMessage;

	constructor(
		private router: Router) { }

	ngOnInit() {
	}
	fileChangeEvent(event: any) {
		let fileReader = new FileReader();
		let file = <File>event.target.files[0];
		if (file) {
			this.fileName = file.name;
			this.model.file = file
			this.isFile = true;
		}
	}
	onSubmit() {
		if (!this.isFile) this.model.file = null;
		var xhr = new XMLHttpRequest();
		// this.importSvc.listenerAddProgress('device', xhr, this.model).subscribe(result => {
		// 	this.progress = result;
		// })
		// this.importSvc.importDevice(xhr).subscribe(result => {
		// 	if (result.status == "OK") {
		// 		// this.alertModel = { type: "success", message: result.message };
		// 		setTimeout(() => this.router.navigate(['/app/device']), 1500);
		// 	} else {
		// 		// this.alertModel = { type: 'danger', message: result.message }
		// 		// setTimeout(() => this.alertModel = null, 1500);
		// 		this.progress = null;
		// 	}
		// });
	}
}
