import { Component, OnInit, ViewChild, TemplateRef } from '@angular/core';
import {
	MapOptions, NavigationControlOptions, OverviewMapControlOptions, ScaleControlOptions, MapTypeControlOptions,
	ControlAnchor, NavigationControlType, MapTypeControlType,
	MarkerOptions, Point
} from 'angular2-baidu-map';
declare var $: any;
declare var INFOBOX_AT_TOP: any;
declare var mapv: any;
import { SituationService } from '../services/situation.service'
import { removeSummaryDuplicates } from '@angular/compiler';
import {$WebSocket} from 'angular2-websocket/angular2-websocket'
import { ISituation } from '../model/ISituation'
import { IMeasures } from '../model/IMeasure'
import { newMidnight } from '../config/style'
@Component({
	selector: 'app-situation',
	templateUrl: './situation.component.html',
	styleUrls: ['./situation.component.scss']
})
export class SituationComponent implements OnInit {
	myMap: any;
	ORIGINAL_LNG = 105.403119;
	ORIGINAL_LAT = 35.588449;

	ORIGINAL_ZOOM = 9;            //地图原始缩放比例
	centerPoint = null;           //地图HOME中心点
	homeZoom = 9;                 //地图HOME缩放比例
	LEVEL_MIN = 5;              //缩放界限
	LEVEL_MAX = 15;
	mapStyleIndex = 0;            //当前地图样式索引
	currentMapStyle = ["midnight", "normal"];   //地图样式
	cityListControl = null;     //城市列表控件
	cityListVisible = false;
	myDrag = null;                //拉框放大控件
	dragZoom = false;
	devMarks = [];              //地图Marks列表，用于点聚合
	devObjList = [];            //仪器对象列表
	areaName = "湖北省";         //初始定位

	showTotalPanel: boolean = true;
	showGeogPanel: boolean = true;
	showUserPanel: boolean = true;
	// showSinglePanel: boolean = true;
	showInfoBox: any;

	enumOnlineStatus = ["下线", "上线", "工作", "报警"];

	public opts: MapOptions;
	public controlOpts: NavigationControlOptions;
	public overviewmapOpts: OverviewMapControlOptions;
	public scaleOpts: ScaleControlOptions;
	public mapTypeOpts: MapTypeControlOptions;
	public markers: Array<{ point: Point; options?: MarkerOptions }>
	public deviceList: Array<any>;
	public alarmList: Array<any>;
	public userList: Array<{userId: string, unitName: string, loginStyle: string, loginTime: Date}>;
	public currentDevice: any;
	public markerList: Array<any> = [];
	public statistics: any = [0, 0, 0];	// 统计个数
	public centre: boolean = false;	//是否归心
	public params: string[] = ['search', 'type', 'icon']
	ws: any;
	public interval: any;

	public measureType = 'O2';
	public devMeasures: ISituation[] = []

	public positionCode: string;
	public iconSize: number;

	public showMapv:boolean = false;
	public mapvLayer1: any = null;	// 热力图层
	public dataSet = null; //热力图数据
	public mapvDatas = [];

	//根据宽度显示toggle-panel
	isLaptop:boolean = false;
	@ViewChild('wrap') 
	wrap: TemplateRef<any>;

	//load 加载	
    public loading = false;

	constructor(private SituationSvc: SituationService) {}
	initValue(){
		var factorValue = 0;
		for(var i = 0; i < this.markerList.length; i++){
			this.mapvDatas.push({
				geometry: {         //地理位置，经纬度以城市中心加随机偏移量生成。实际数据用仪器经纬度
					type: 'Point',
					coordinates: [this.markerList[i].getPosition().lng, this.markerList[i].getPosition().lat]
				},
				count: factorValue,      //绘图值 实际数据用仪器测量数据
			});
		}
		this.dataSet = new mapv.DataSet(this.mapvDatas);
	}
	updateValue(devMaker, value) {
		if(this.mapvDatas.length) {
			let index = this.markerList.indexOf(devMaker.marker);
			this.mapvDatas[index].count = value*5;	//没有安照实际数据展示，*5是为了显示效果
			if(this.dataSet == null) {   // 构造数据,能收到实时数据时，以实时数据显示
				this.dataSet = new mapv.DataSet(this.mapvDatas);
			}
			else{
				this.dataSet.set(this.mapvDatas); // 修改数据
			}
		}
	}
	showMapvStatus(devMaker?:any, value?:any) {
		if(!devMaker || !value) {
			this.initValue();
		} else {
			this.updateValue(devMaker, value)
		}
		var options = {
			size: 60,       //圆直径
			gradient: {0.25: "rgb(0,0,255)", 0.55: "rgb(0,255,0)", 0.85: "yellow", 1.0: "rgb(255,0,0)"},//颜色梯度
			max: 300,       //最大值
			draw: 'heatmap' //图形类别
		}
		if(this.mapvLayer1 != null){
			this.mapvLayer1.show();
		}
		else {
			this.mapvLayer1 = new mapv.baiduMapLayer(this.myMap, this.dataSet, options);
		}
	}
	heatMapChange(result) {
		if(result) {
			this.showMapvStatus();
		} else {
			if(this.mapvLayer1) this.mapvLayer1.hide();
		}
		this.showMapv = result;
	}
	ngOnInit() {		
		// 笔记本下初始化页面 
		// this.isLaptop = (<any>this.wrap).nativeElement.clientHeight < 950;
		// if(this.isLaptop)  {	
		// 	this.showTotalPanel = false;
		// 	this.showGeogPanel = false;
		// 	this.showUserPanel = false;
		// }
	
		// 检测是否进行过搜索
		var positionCode = sessionStorage.getItem("positionCode");
		if(positionCode) {
			this.positionCode = positionCode;
		}
		this.init();
		this.SituationSvc.getalarm(this.positionCode).subscribe(result => {
			if (result != null) {
				this.alarmList = result.datas;
			}
		})
		this.SituationSvc.getusers(this.positionCode).subscribe(result => {
			if(result != null) {
				this.userList = result;
			}
		})
		// this.ws = new $WebSocket(`ws://${location.hostname}:3006`);
		// this.ws = new $WebSocket(`ws://${location.host}/hnty/real`);
		// this.ws.onMessage(
		// 	(msg: MessageEvent)=> {
		// 		var result = JSON.parse(msg.data)
		// 		if(result.measures) {
		// 			// 匹配窗口
		// 			var currentIndex;
		// 			currentIndex = this.devMeasures.map(item => {return item.deviceId}).indexOf(result.deviceId);
		// 			// 更新图表数据
		// 			if(currentIndex > -1) {
		// 				this.devMeasures[currentIndex].measures.forEach(item => {
		// 					result.measures.map(data => {
		// 						if(data.key == item.key) Object.assign(item, data);
		// 					})
		// 				})
		// 			}
		// 			// 更新label数据
		// 			this.updateLabel(result);
		// 		}
		// 	},
		// 	{autoApply: false}
		// );
	}

	ngOnDestroy() {
		if(this.interval) clearInterval(this.interval);
	}

	//更新实时运行情况panel，用户panel，报警panel
	updatePanel() {
		this.SituationSvc.getalarm(this.positionCode).subscribe(result => {
			if (result != null) {
				this.alarmList = result.datas;
			}
		})
		this.SituationSvc.getusers(this.positionCode).subscribe(result => {
			if(result != null) {
				this.userList = result;
			}
		})
		this.SituationSvc.getCurrentSituation(this.positionCode).subscribe(result => {
			sessionStorage.setItem('situationDeviceList', JSON.stringify(result));
			// 新获取仪器列表与老仪器列表交集，取的新数据
			var exsitDevs = result.datas.filter(device => this.deviceList.map(item => item.deviceId).indexOf(device.deviceId) > -1);			
			// 移除的仪器列表，取的老数据
			var removeDevs = this.deviceList.filter(device => exsitDevs.map(item => item.deviceId).indexOf(device.deviceId)<0);
			// 新增的仪器列表，取的新数据
			var addDevs = result.datas.filter(device => exsitDevs.map(item => item.deviceId).indexOf(device.deviceId) < 0);
			// 已存在的仪器列表，进行数据更新
			if(exsitDevs.length) {		
				exsitDevs.forEach(dev => {
					let oldDevice = this.deviceList.filter(item => item.deviceId == dev.deviceId)[0];
					if(dev && oldDevice.onlineState != dev.onlineState) {
						oldDevice.onlineState = dev.onlineState
						let devMarker = this.searchDevMarkerById(oldDevice.deviceId);
						let icon = devMarker.marker.getIcon();
						if(devMarker && devMarker.marker) {
							let imgName = "";
							switch (oldDevice.onlineState) {                 //不同状态用不同图标 尺寸32*32
								case this.enumOnlineStatus[0]:
									imgName = "/assets/image/new-icon/offline.svg";
									break;
								case this.enumOnlineStatus[1]:
									imgName = "/assets/image/new-icon/online.svg";
									break;
								case this.enumOnlineStatus[2]:
									imgName = "/assets/image/new-icon/working.svg";
									break;
								case this.enumOnlineStatus[3]:
									imgName = "/assets/image/new-icon/alert.svg";
									break;
								default:
									break;
							}
							icon.setImageUrl(imgName);
							devMarker.marker.setIcon(icon);
						}
					}
				})
			}
			// 移除老数据更新地图
			if(removeDevs.length) {		
				removeDevs.forEach(oldDevice => {
					let devMarker = this.searchDevMarkerById(oldDevice.deviceId);
					if(devMarker && devMarker.marker) {
						// 从仪器列表移除老仪器
						let deviceListIndex = this.deviceList.findIndex(dev => dev.deviceId == devMarker.device.deviceId);
						this.deviceList.splice(deviceListIndex, 1);
						// 从markers列表移除老marker
						let markersIndex = this.markers.findIndex(marker => marker.point.lat == devMarker.marker.getPosition().lat && marker.point.lng == devMarker.marker.getPosition().lng);
						this.markers.splice(markersIndex, 1);
						// 从markerList列表移除老marker
						let markerListIndex = this.markerList.findIndex(marker => marker == devMarker.marker);
						this.markerList.splice(markerListIndex, 1);
					}
				})
			}
			// 添加新数据更新地图
			if(addDevs.length) {
				addDevs.forEach(newDevice => {
					// deviceList 更新
					this.deviceList.push(newDevice);
					// markers更新，然后loadMarker更新markerList
					this.addMarker(newDevice, this.markers);
				})
			}
		})
	}

	// 更新地图
	refreshMap(code?:string) {
		if(this.interval) clearInterval(this.interval);
		this.devMeasures = [];
		// this.userList = [];
		this.alarmList = [];
		if(this.showInfoBox) {
			this.showInfoBox.close();
		}
		let situationDeviceList = sessionStorage.getItem("situationDeviceList");
		if(situationDeviceList) {
			this.initMap(JSON.parse(situationDeviceList));
			setTimeout(() => {this.loading = false}, 5000)
			// this.interval = setInterval(() => {
			// 	this.updatePanel();
			// }, 10000)
		} else {
			this.SituationSvc.getCurrentSituation(code).subscribe(result => {
				if (result != null) {
					sessionStorage.setItem('situationDeviceList', JSON.stringify(result));
					this.initMap(result);
					setTimeout(() => {this.loading = false}, 5000)
					this.interval = setInterval(() => {
						this.updatePanel();
					}, 10000)
				}
			})
		}
		this.SituationSvc.getalarm(this.positionCode).subscribe(result => {
			if (result != null) {
				this.alarmList = result.datas;
			}
		})
		this.SituationSvc.getusers(this.positionCode).subscribe(result => {
			if(result != null) {
				this.userList = result;
			}
		})
	}

	ngAfterViewInit() {		
		$('body').append(`
			<script src="/assets/plugin/InfoBox_min.js"></script>
			<script src="/assets/plugin/TextIconOverlay_min.js"></script>
			<script src="/assets/plugin/MarkerClusterer_min.js"></script>
		`)
	 }

	init() {
		this.opts = {
			centerAndZoom: {
				lng: 114.314933,
				lat: 30.606137,
				zoom: 7
			},
			enableScrollWheelZoom: true
		};

		this.controlOpts = {
			anchor: ControlAnchor.BMAP_ANCHOR_TOP_LEFT,
			type: NavigationControlType.BMAP_NAVIGATION_CONTROL_LARGE,
			offset: { width: 50, height: 150 }
		}

		this.overviewmapOpts = {
			anchor: ControlAnchor.BMAP_ANCHOR_BOTTOM_RIGHT,
			isOpen: true
		}

		this.scaleOpts = {
			anchor: ControlAnchor.BMAP_ANCHOR_BOTTOM_LEFT
		}

		this.mapTypeOpts = {
			type: MapTypeControlType.BMAP_MAPTYPE_CONTROL_HORIZONTAL
		}
	}

	// 初始化地图
	initMap(result: any) {
		this.deviceList = result.datas;
		this.statisticsDevice();
		this.currentDevice = this.deviceList[0]
		this.addMarkers(this.deviceList.slice(0,100));
		setTimeout(() => this.addMarkers(this.deviceList.slice(100, 200)), 2000);
		setTimeout(() => this.addMarkers(this.deviceList.slice(200, 300)), 2000);
		setTimeout(() => this.addMarkers(this.deviceList.slice(300, 400)), 2000);
		// setTimeout(() => this.addMarkers(this.deviceList.slice(400, 500)), 2000);
		// setTimeout(() => this.addMarkers(this.deviceList.slice(500, 600)), 2000);
		// setTimeout(() => this.addMarkers(this.deviceList.slice(600, 700)), 2000);
		// setTimeout(() => this.addMarkers(this.deviceList.slice(700, 800)), 2000);
		// setTimeout(() => this.addMarkers(this.deviceList.slice(800, 900)), 2000);
		// setTimeout(() => this.addMarkers(this.deviceList.slice(900, 2000)), 2000);
		// setTimeout(() => this.addMarkers(this.deviceList.slice(1000, 1100)), 2000);
		// setTimeout(() => this.addMarkers(this.deviceList.slice(1100, 1200)), 2000);
		// setTimeout(() => this.addMarkers(this.deviceList.slice(1200, 1300)), 2000);
		// setTimeout(() => this.addMarkers(this.deviceList.slice(1300, 1400)), 2000);
		// setTimeout(() => this.addMarkers(this.deviceList.slice(1400, 1500)), 2000);
		// setTimeout(() => this.addMarkers(this.deviceList.slice(1500, 1600)), 2000);
		// setTimeout(() => this.addMarkers(this.deviceList.slice(1600, 1700)), 2000);
		// setTimeout(() => this.addMarkers(this.deviceList.slice(1700, 1800)), 2000);
		// setTimeout(() => this.addMarkers(this.deviceList.slice(1800, 1900)), 2000);
		// setTimeout(() => this.addMarkers(this.deviceList.slice(1900, 2000)), 2000);
		// setTimeout(() => this.addMarkers(this.deviceList.slice(2000, 2100)), 2000);
	}

	// 添加markers
	addMarkers(deviceList) {
		var that = this;
		this.markers = this.markers && this.markers.length? this.markers:[] ;
		// 这里的不是
		deviceList.forEach((item) => {
			this.addMarker(item, that.markers);
		})
	}

	// 添加单个marker
	addMarker(item, markers) {
		var convertor = new window.BMap['Convertor']()
		let imgName = "";
		switch (item.onlineState) {                 //不同状态用不同图标 尺寸32*32
			case this.enumOnlineStatus[0]:
				imgName = "/assets/image/new-icon/offline.svg";
				break;
			case this.enumOnlineStatus[1]:
				imgName = "/assets/image/new-icon/online.svg";
				break;
			case this.enumOnlineStatus[2]:
				imgName = "/assets/image/new-icon/working.svg";
				break;
			case this.enumOnlineStatus[3]:
				imgName = "/assets/image/new-icon/alert.svg";
				break;
			default:
				break;
		}
		// (function(item) {
		// 	convertor.translate([new window.BMap.Point(item.longitude, item.latitude)], 1, 5, (data) => {
		// 		item.longitude = data.points[0].lng;
		// 		item.latitude = data.points[0].lat;
		// 		markers.push({
		// 			options: {
		// 				icon: {
		// 					imageUrl: imgName,
		// 					size: {
		// 						height: 48,
		// 						width: 48
		// 					},
		// 					anchor: {
		// 						width: 0,
		// 						height:48
		// 					}
		// 				},
		// 				enableMassClear: true
		// 			},
		// 			point: {
		// 				lng: <number>item.longitude,
		// 				lat: <number>item.latitude
		// 			}
		// 		})
		// 	})
		// })(item);
		// 为解决百度地图gps转换超标临时方案，将产生marker定位不准问题
		markers.push({
			options: {
				icon: {
					imageUrl: imgName,
					size: {
						height: 48,
						width: 48
					},
					anchor: {
						width: 0,
						height:48
					}
				},
				enableMassClear: true
			},
			point: {
				lng: <number>item.longitude,
				lat: <number>item.latitude
			}
		})
	}

	// info窗
	public showWindow({ e, marker, map }: any, index): void {
		// var deviceInfo = this.deviceList[index];
		// console.dir(deviceInfo)
		var deviceInfo = this.searchDevMarkerByPosition(marker.getPosition()).device;
		// console.dir(customer.device)
		var href = `http://47.92.28.186:8080/hnty/a/monitor/device/view?id=${deviceInfo.deviceId}`;
		var target = "target='_blank'";
		// if(deviceInfo.deviceId != 'LH12A08885739X') {
		// 	href="/app/situation/demo";
		// 	target = "";
		// } 
		var content = `
					<div class="infowindow">
						<ul class="infoWindow-list">
							<li class="row no-gutters">
								<div class="col-md-4 text-right key">仪器序号：</div>
								<div class="col text-left value">${deviceInfo.deviceId}</div>
							</li>
							<li class="row no-gutters">
								<div class="col-md-4 text-right key">仪器型号：</div>
								<div class="col text-left value">${deviceInfo.deviceType}</div>
							</li>
							<li class="row no-gutters">
								<div class="col-md-4 text-right key">生产厂家：</div>
								<div class="col text-left value">青岛崂应</div>
							</li>
							<li class="row no-gutters">
								<div class="col-md-4 text-right key">购买单位：</div>
								<div class="col text-left value">${deviceInfo.devicePurUnitName}</div>
							</li>
							<li class="row no-gutters">
								<div class="col-md-4 text-right key">健康状态：</div>
								<div class="col text-left value">${deviceInfo.healthState}</div>
							</li>
							<li class="divider"></li>
							<li class="row no-gutters">
								<a type="button" class="btn infowindowButton" href="${href}" style="color:#fff">实时工步</a>
								<a type="button" class="btn infowindowButton" href="/app/history?instrumentId=${deviceInfo.deviceId}" style="color:#fff">历史溯源</a>
							</li>
						</ul>						
					</div>`;
		var infoBox = new (<any>window.BMapLib).InfoBox(this.myMap, content, {
			offset:{height:0,width:0},
			boxStyle:{
				border: '1px solid #00a6fe',
				borderRadius: '12px',				
				background: 'rgba(12,58,104, 0.75)',
				width: '250px',
				height: '256px'
			},
			boxClass:"infobox",
			closeIconMargin: "5px 8px 0 0",
			closeIconUrl: "assets/image/close.png",
			enableAutoPan: true,
			align: INFOBOX_AT_TOP
		});
		if(this.showInfoBox) {
			this.showInfoBox.close();
		}
		this.showInfoBox = infoBox;
		this.showInfoBox.open(marker)
		this.myMap.removeOverlay(marker.getIcon());
		this.selectDevice(deviceInfo.deviceId, null, marker);
	}
	loadMarker(marker: any,i) {
		this.markerList[i] = marker;
		if(this.markerList.length && this.markerList.length && !(this.markerList.length %100)) {
			var intervalCluster = setInterval(() => {
				if((<any>window.BMapLib).MarkerClusterer) {
					var markerClusterer = new (<any>window.BMapLib).MarkerClusterer(this.myMap, {markers:this.markerList});
					clearInterval(intervalCluster);
				}
			}, 500);
		}
		console.log(this.markerList.length)
	}
	loadMap(map: any) {
		this.myMap = map;
		this.myMap.setMapStyle({styleJson:newMidnight})
		// this.gotoArea(this.areaName);
		this.refreshMap();
	}

	// 选择仪器
	public selectDevice(id?: number, index?: any, marker?: any) {
		if (id == null) {
			id = this.deviceList[index].deviceId
		}
		this.currentDevice = this.deviceList.filter(item => item && item.deviceId == id)[0];
		if (!this.currentDevice) return;
		if(marker) {
			var currentMarker = marker;
		} else {
			var currentMarker = this.markerList.filter(item => {
				return item.getPosition().lng == this.currentDevice.longitude && item.getPosition().lat == this.currentDevice.latitude
			})[0];
		}
		this.clearAllSelectDevice();
		if (currentMarker != null) {
			let icon = currentMarker.getIcon();
			let imgName = "";
			switch (icon.imageUrl) {                 //不同状态用不同图标 尺寸32*32
				case "/assets/image/new-icon/offline.svg":
					imgName = "/assets/image/new-icon/offline-select.svg";
					break;
				case "/assets/image/new-icon/online.svg":
					imgName = "/assets/image/new-icon/online-select.svg";
					break;
				case "/assets/image/new-icon/working.svg":
					imgName = "/assets/image/new-icon/working-select.svg";
					break;
				case "/assets/image/new-icon/alert.svg":
					imgName = "/assets/image/new-icon/alert-select.svg";
					break;
				default:
					imgName = icon.imageUrl;
					break;
			}
			icon.setImageUrl(imgName);
			currentMarker.setIcon(icon);
			// this.myMap.removeOverlay(currentMarker);
			if(this.centre) this.myMap.panTo(currentMarker.getPosition())
			if(this.currentDevice.onlineState == this.enumOnlineStatus[1] || this.currentDevice.onlineState == this.enumOnlineStatus[2]) this.updateDevMeasures(this.currentDevice);
			// this.updateDevMeasures(this.currentDevice);
		}
	}

	// 清除icon的选中状态
	clearAllSelectDevice() {
		this.markerList.forEach(item => {
			let icon = item.getIcon();
			let imgName = "";
			switch (icon.imageUrl) {                 //不同状态用不同图标 尺寸32*32
				case "/assets/image/new-icon/offline-select.svg":
					imgName = "/assets/image/new-icon/offline.svg";
					break;
				case "/assets/image/new-icon/online-select.svg":
					imgName = "/assets/image/new-icon/online.svg";
					break;
				case "/assets/image/new-icon/working-select.svg":
					imgName = "/assets/image/new-icon/working.svg";
					break;
				case "/assets/image/new-icon/alert-select.svg":
					imgName = "/assets/image/new-icon/alert.svg";
					break;
				default:
					imgName = icon.imageUrl;
					break;
			}
			icon.setImageUrl(imgName);
			item.setIcon(icon);
		})
	}

	// 统计仪器在线情况数据
	statisticsDevice() {
		var works = this.deviceList.filter(device => device.onlineState == this.enumOnlineStatus[2]).length;
		var onlines = this.deviceList.filter(device => device.onlineState == this.enumOnlineStatus[1]).length;
		var offlines = this.deviceList.filter(device => device.onlineState == this.enumOnlineStatus[0]).length;
		this.statistics = [works, onlines, offlines];
	}

	// 响应header搜索及设置变化
	changeItem(param) {
		switch (param.type) {
		  case this.params[0]:
			// 根据code重新搜索
			this.positionCode = param.value;
			sessionStorage.setItem("positionCode", this.positionCode);
			this.loading = true;
		    this.refreshMap(param.value);
			break;
		  case this.params[1]:
			// 切换marker的label显示的数据类型
			this.measureType = param.value;
			this.updateLabelType();
			break;
		  case this.params[2]:
			// 重新设置icon的尺寸
			this.iconSize = param.value;
			this.updateIconSize();
			break;
		  default: 
			break;
		}
	}

	// 根据位置搜索devMarker
	searchDevMarkerByPosition(position: {lng: number, lat: number}) {
		let device = this.deviceList.filter(item => position.lng == item.longitude && position.lat == item.latitude)[0];
		if(device) {
			let marker = this.markerList.filter(item => {
				return position.lng == item.getPosition().lng && position.lat == item.getPosition().lat
			})[0];
			if(marker) {
				return {
					device: device,
					marker: marker
				}
			}
			return null;
		}
		return null;
	}
	// 根据Id搜索devMarker
	searchDevMarkerById(id:string) {
		let device = this.deviceList.filter(item => item.deviceId == id)[0];
		if(device) {
			let marker = this.markerList.filter(item => {
				return item.getPosition().lng == device.longitude && item.getPosition().lat == device.latitude
			})[0];
			if(marker) {
				return {
					device: device,
					marker: marker
				}
			}
			return null;
		}
		return null;
	}

	// 根据websocket推送更新label
	updateLabel(message: IMeasures) {
		// 根据type设置显示测量数据
		var devMarker = this.searchDevMarkerById(message.deviceId);
		if(devMarker) {
			var labelMeasure = message.measures.filter(item => item.key == this.measureType)[0]
			if(labelMeasure) {
				var label = devMarker.marker.getLabel();
				var newLabel = new window.BMap.Label(labelMeasure.value, <any>{ offset: new window.BMap.Size(4, 8) });
				newLabel.setStyle({background: 'transparent', border:0})
				this.myMap.removeOverlay(label);
				devMarker.marker.setLabel(newLabel);
				if(this.mapvLayer1 && this.showMapv) this.showMapvStatus(devMarker, labelMeasure.value)
			}
		}
	}

	// 关闭图表窗口或选择仪器
	closeOrSelectWindow(obj: {type: string, value: number}, i: number) {
		if(obj.type == 'close') {
			this.devMeasures.splice(i, 1);
		} else if(obj.type == 'select') {
			this.selectDevice(obj.value)
		}
	}

	// 检查更新窗口数
	updateDevMeasures(device: ISituation) {
		var length = this.devMeasures.filter(item => item.deviceId == device.deviceId).length;
		if(!length) {
			this.devMeasures.push(device);
		}
	}

	updateIconSize() {
		this.markerList.map(item => {
			item.getIcon().setSize(new window.BMap.Size(this.iconSize, this.iconSize));
			item.setIcon(item.getIcon());
		})
	}

	updateLabelType() {
		this.markerList.map(item => {
			this.myMap.removeOverlay(item.getLabel());
		})
		if(this.mapvLayer1) {
			this.mapvLayer1.hide();
			this.showMapvStatus();
		}	
	}

	// 添加湖北省范围覆盖物
	gotoArea(areaName) {
		var bdary = new (<any>window.BMap).Boundary();
		bdary.get(areaName, function(rs){       //获取行政区域
			// this.myMap.clearOverlays();        //清除地图覆盖物
			var count = rs.boundaries.length; //行政区域的点有多少个
			if (count === 0) {
				console.log('未能获取当前输入行政区域');
				return ;
			}
			var pointArray = [];
			for (var i = 0; i < count; i++) {
				var ply:any = new window.BMap.Polygon(rs.boundaries[i],
					{strokeWeight: 4, strokeColor: "#0466fd", strokeOpacity:0.8, fillColor:"#dde5fa", fillOpacity: 0.1}); //建立多边形覆盖物
				this.myMap.addOverlay(ply);  //添加覆盖物
				pointArray = pointArray.concat(ply.getPath());
			}
			this.myMap.setViewport(pointArray);    //调整视野
			// this.myMap.setZoom(9);
	
			// DropdownSetHome();
		}.bind(this));
	}
}
