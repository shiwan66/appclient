import { async, ComponentFixture, TestBed } from '@angular/core/testing';

import { SituationDemoComponent } from './situation-demo.component';

describe('SituationDemoComponent', () => {
  let component: SituationDemoComponent;
  let fixture: ComponentFixture<SituationDemoComponent>;

  beforeEach(async(() => {
    TestBed.configureTestingModule({
      declarations: [ SituationDemoComponent ]
    })
    .compileComponents();
  }));

  beforeEach(() => {
    fixture = TestBed.createComponent(SituationDemoComponent);
    component = fixture.componentInstance;
    fixture.detectChanges();
  });

  it('should create', () => {
    expect(component).toBeTruthy();
  });
});
