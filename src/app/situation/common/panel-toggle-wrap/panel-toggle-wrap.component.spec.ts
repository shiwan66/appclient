import { async, ComponentFixture, TestBed } from '@angular/core/testing';

import { PanelToggleWrapComponent } from './panel-toggle-wrap.component';

describe('PanelToggleWrapComponent', () => {
  let component: PanelToggleWrapComponent;
  let fixture: ComponentFixture<PanelToggleWrapComponent>;

  beforeEach(async(() => {
    TestBed.configureTestingModule({
      declarations: [ PanelToggleWrapComponent ]
    })
    .compileComponents();
  }));

  beforeEach(() => {
    fixture = TestBed.createComponent(PanelToggleWrapComponent);
    component = fixture.componentInstance;
    fixture.detectChanges();
  });

  it('should create', () => {
    expect(component).toBeTruthy();
  });
});
