import { Component, OnInit, Input, Output, EventEmitter } from '@angular/core';

@Component({
  selector: 'app-panel-toggle-wrap',
  templateUrl: './panel-toggle-wrap.component.html',
  styleUrls: ['./panel-toggle-wrap.component.scss']
})
export class PanelToggleWrapComponent implements OnInit {
  @Input() isLaptop: boolean;
  @Input() showTotalPanel: boolean;
  @Input() showGeogPanel: boolean;
  @Input() showUserPanel: boolean;
  @Output() showTotalPanelChange: EventEmitter<boolean> = new EventEmitter();
  @Output() showGeogPanelChange: EventEmitter<boolean> = new EventEmitter();
  @Output() showUserPanelChange: EventEmitter<boolean> = new EventEmitter();
  actived: boolean = true;
  icons: any[] = [
    {title: '下线', url: '/assets/image/new-icon/offline.svg'},
    {title: '上线', url: '/assets/image/new-icon/online.svg'},
    {title: '工作', url: '/assets/image/new-icon/working.svg'},
    // {title: '报警', url: '/assets/image/new-icon/alert.svg'},
    {title: '选中', url: '/assets/image/new-icon/select.svg'}
  ]
  constructor() { }

  ngOnInit() {
  }

  togglePanel(name: string) {
    switch(name) {
      case 'totalRealtimePanel':
        this.showTotalPanel = !this.showTotalPanel;
        this.showTotalPanelChange.emit(this.showTotalPanel);
        if(this.isLaptop) {
          this.showGeogPanel = false;
          this.showGeogPanelChange.emit(this.showGeogPanel);
          this.showUserPanel = false;
          this.showUserPanelChange.emit(this.showUserPanel);
        }
        break;
      case 'geogPanel':
        this.showGeogPanel = !this.showGeogPanel;
        this.showGeogPanelChange.emit(this.showGeogPanel);
        if(this.isLaptop) {
          this.showTotalPanel = false;
          this.showTotalPanelChange.emit(this.showTotalPanel);
          this.showUserPanel = false;
          this.showUserPanelChange.emit(this.showUserPanel);
        }
        break;
      case 'userPanel':
        this.showUserPanel = !this.showUserPanel;
        this.showUserPanelChange.emit(this.showUserPanel);
        if(this.isLaptop) {
          this.showTotalPanel = false;
          this.showTotalPanelChange.emit(this.showTotalPanel);
          this.showGeogPanel = false;
          this.showGeogPanelChange.emit(this.showGeogPanel);
        }
        break;
      default: 
        break;
    }
  }

}
