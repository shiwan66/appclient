import { Component, OnInit, Input, Output, EventEmitter } from '@angular/core';

@Component({
  selector: 'app-user-panel',
  templateUrl: './user-panel.component.html',
  styleUrls: ['./user-panel.component.scss']
})
export class UserPanelComponent implements OnInit {
  @Input() userList?: Array<{userId?: string, unitName?: string, loginStyle?: string, loginTime?: Date}> = [{}];
  @Input() currentDivece?: any = {};
  @Input() isLaptop: boolean;
  @Output() selectDevice: EventEmitter<any> = new EventEmitter();
  constructor() { }

  ngOnInit() {
  }

  clickuser(user) {
    
  }

}
