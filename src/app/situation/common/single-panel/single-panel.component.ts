import { Component, OnInit, Input, Output, EventEmitter } from '@angular/core';

@Component({
  selector: 'app-single-panel',
  templateUrl: './single-panel.component.html',
  styleUrls: ['./single-panel.component.scss']
})
export class SinglePanelComponent implements OnInit {
  @Input() measure?: any;
  @Input() index?: number;
  @Output() removeSelect: EventEmitter<any> = new EventEmitter();
  constructor() { }

  ngOnInit() {
  }

  close() {
    this.removeSelect.emit(this.index)
  }

}
