import { Component, OnInit, Input, Output, EventEmitter } from '@angular/core';
import { AddressDataChinaService } from 'ngx-address/data/china';

@Component({
  selector: 'app-situation-header',
  templateUrl: './situation-header.component.html',
  styleUrls: ['./situation-header.component.scss']
})
export class SituationHeaderComponent implements OnInit {
  @Input() statistics?: any[] = [];
  centreValue: boolean;
  @Input()
  get centre(): boolean {
    return this.centreValue;
  }
  set centre(val: boolean) {
    this.centreValue = val;
    this.centreChange.emit(this.centreValue);
  }
  @Output() centreChange: EventEmitter<boolean> = new EventEmitter();

  heatMap:boolean = false;
  @Output() heatMapChange: EventEmitter<boolean> = new EventEmitter();
  public code: string;
  public codeName: string;
  public opt: any;
  actived: boolean = true;
  @Output() changeItem: EventEmitter<{ type: string, value: any }> = new EventEmitter();
  statusList: any[] = [
    {
      title: '工作',
      value: 0
    },
    {
      title: '在线',
      value: 0
    },
    {
      title: '下线',
      value: 0
    }
  ]
  type: string = 'O2';
  types: any[] = [
    {
      label: 'O2浓度',
      value: 'O2'
    },
    {
      label: 'CO2浓度',
      value: 'CO2'
    },
    {
      label: 'SO2浓度',
      value: 'SO2'
    },
    {
      label: 'NO浓度',
      value: 'NO'
    },
    {
      label: 'NO2浓度',
      value: 'NO2'
    },
    {
      label: 'NOX浓度',
      value: 'NOX'
    },
    {
      label: 'CO浓度',
      value: 'CO'
    },
    {
      label: 'H2S浓度',
      value: 'H2S'
    },
    {
      label: 'PM2.5浓度',
      value: 'PM25'
    },
    {
      label: 'PM10浓度',
      value: 'PM10'
    }
  ]
  icon: number = 48;
  icons: any[] = [
    {
      label: '小图标',
      value: 36
    },
    {
      label: '中图标',
      value: 48
    },
    {
      label: '大图标',
      value: 60
    },
  ]
  @Input() params?:string[];
  constructor(private china: AddressDataChinaService) {
    this.opt = {
      jumps: this.china.getJumps(),
      data: this.china.getData.bind(this.china)
    };
  }
  heatChange() {
    this.heatMapChange.emit(this.heatMap);
  }
  ngOnInit() {    
    var positionCode = sessionStorage.getItem("positionCode");
    if(positionCode) {
      this.code = positionCode;
    }
  }
  changeParams(param) {
    switch (param) {
      case this.params[0]:
        this.changeItem.emit({ type: this.params[0], value: this.code });
        break;
      case this.params[1]:
        this.changeItem.emit({ type: this.params[1], value: this.type });
        break;
      case this.params[2]:
        this.changeItem.emit({ type: this.params[2], value: this.icon });
        break;
      default: 
        break;
    }
  }
  onCustomSelected(result) {
    if(result) {
      this.codeName = result.paths.map(item => item.name).join("");
    }
  }
}
