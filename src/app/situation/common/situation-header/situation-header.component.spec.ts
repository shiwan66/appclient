import { async, ComponentFixture, TestBed } from '@angular/core/testing';

import { SituationHeaderComponent } from './situation-header.component';

describe('SituationHeaderComponent', () => {
  let component: SituationHeaderComponent;
  let fixture: ComponentFixture<SituationHeaderComponent>;

  beforeEach(async(() => {
    TestBed.configureTestingModule({
      declarations: [ SituationHeaderComponent ]
    })
    .compileComponents();
  }));

  beforeEach(() => {
    fixture = TestBed.createComponent(SituationHeaderComponent);
    component = fixture.componentInstance;
    fixture.detectChanges();
  });

  it('should create', () => {
    expect(component).toBeTruthy();
  });
});
