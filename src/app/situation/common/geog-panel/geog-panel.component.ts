import { Component, OnInit, Input, Output, EventEmitter } from '@angular/core';

@Component({
  selector: 'app-geog-panel',
  templateUrl: './geog-panel.component.html',
  styleUrls: ['./geog-panel.component.scss']
})
export class GeogPanelComponent implements OnInit {
  @Input() alarmList?: Array<any> = [{}];
  @Input() currentDivece?: any = {};
  @Input() isLaptop: boolean;
  @Output() selectDevice: EventEmitter<any> = new EventEmitter();
  constructor() { }

  ngOnInit() {
  }

  clickAlarm(alarm) {
    this.selectDevice.emit(alarm.instrumentID);
  }

}
