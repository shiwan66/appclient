import { async, ComponentFixture, TestBed } from '@angular/core/testing';

import { GeogPanelComponent } from './geog-panel.component';

describe('GeogPanelComponent', () => {
  let component: GeogPanelComponent;
  let fixture: ComponentFixture<GeogPanelComponent>;

  beforeEach(async(() => {
    TestBed.configureTestingModule({
      declarations: [ GeogPanelComponent ]
    })
    .compileComponents();
  }));

  beforeEach(() => {
    fixture = TestBed.createComponent(GeogPanelComponent);
    component = fixture.componentInstance;
    fixture.detectChanges();
  });

  it('should create', () => {
    expect(component).toBeTruthy();
  });
});
