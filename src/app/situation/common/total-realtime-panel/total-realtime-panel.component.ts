import { Component, OnInit, Input, Output, EventEmitter } from '@angular/core';

@Component({
  selector: 'app-total-realtime-panel',
  templateUrl: './total-realtime-panel.component.html',
  styleUrls: ['./total-realtime-panel.component.scss']
})
export class TotalRealtimePanelComponent implements OnInit {
  @Input() deviceList?: Array<any> = [{}];
  @Input() currentDivece?: any = {};
  @Output() selectDevice: EventEmitter<any> = new EventEmitter();
  constructor() { }

  ngOnInit() {
  }

  clickDevice(device) {
    this.selectDevice.emit(device.deviceId);
  }

}
