import { async, ComponentFixture, TestBed } from '@angular/core/testing';

import { TotalRealtimePanelComponent } from './total-realtime-panel.component';

describe('TotalRealtimePanelComponent', () => {
  let component: TotalRealtimePanelComponent;
  let fixture: ComponentFixture<TotalRealtimePanelComponent>;

  beforeEach(async(() => {
    TestBed.configureTestingModule({
      declarations: [ TotalRealtimePanelComponent ]
    })
    .compileComponents();
  }));

  beforeEach(() => {
    fixture = TestBed.createComponent(TotalRealtimePanelComponent);
    component = fixture.componentInstance;
    fixture.detectChanges();
  });

  it('should create', () => {
    expect(component).toBeTruthy();
  });
});
