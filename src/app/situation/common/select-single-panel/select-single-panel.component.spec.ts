import { async, ComponentFixture, TestBed } from '@angular/core/testing';

import { SelectSinglePanelComponent } from './select-single-panel.component';

describe('SelectSinglePanelComponent', () => {
  let component: SelectSinglePanelComponent;
  let fixture: ComponentFixture<SelectSinglePanelComponent>;

  beforeEach(async(() => {
    TestBed.configureTestingModule({
      declarations: [ SelectSinglePanelComponent ]
    })
    .compileComponents();
  }));

  beforeEach(() => {
    fixture = TestBed.createComponent(SelectSinglePanelComponent);
    component = fixture.componentInstance;
    fixture.detectChanges();
  });

  it('should create', () => {
    expect(component).toBeTruthy();
  });
});
