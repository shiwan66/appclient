import { Component, OnInit, Input, Output, EventEmitter } from '@angular/core';

@Component({
  selector: 'app-select-single-panel',
  templateUrl: './select-single-panel.component.html',
  styleUrls: ['./select-single-panel.component.scss']
})
export class SelectSinglePanelComponent implements OnInit {
  @Input("measure") data?: any;
  // @Input() deviceId?: string;
  @Input() currentDevice?:any = {};
  @Input() devMeasure?: any = {};
  @Output() removeSelect: EventEmitter<any> = new EventEmitter();
  constructor() { }

  ngOnInit() {
  }

  close(result:any) {
    this.removeSelect.emit(result)
  }

  format(name) {
    if(name == 'PM25') {
      return 'PM2.5'
    }
    return name;
  }
}
