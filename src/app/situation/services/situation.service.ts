import { Injectable } from '@angular/core';
import { Observable } from 'rxjs';
import { Http, Headers, Response, RequestOptionsArgs } from '@angular/http';
import { Router } from '@angular/router';
import 'rxjs/add/operator/map';
@Injectable()
export class SituationService {

  serverUrl = '/'+location.pathname.split("/")[1]+'/a/';
  constructor(private http: Http, private router: Router) { }
  
  //环境态势测试数据获取
  getCurrentSituation(code?: string): Observable<any> {
    let url = '/a/situation/currentsituation/datas';
    if(code) url+=`?positionCode=${code}`;
    return this.http.get(url).map(result => {
      return result.json();
    })
  }  
  
  //环境态势报警数据获取
  getalarm(code?: string): Observable<any> {
    let url = '/a/info/alarm/datas';
    if(code) url+=`?positionCode=${code}`;
    let username = sessionStorage.getItem("username");
    if(username) {
      if(code) url+=`&username=${username}`;
      else url+=`?username=${username}`;
    }
    return this.http.get(url).map(result => {
      return result.json();
    })
  }

  getusers(code?: string): Observable<any> {
    let url = '/a/user/show_user';
    if(code) url+=`?positionCode=${code}`;
    return this.http.get(url).map(result => {
      return [].concat(...result.json());
    })
  }

}
