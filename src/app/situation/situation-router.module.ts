import { NgModule } from '@angular/core';
import { RouterModule, Routes } from "@angular/router";
import { SituationComponent } from './situation/situation.component';
import { SituationDemoComponent } from './situation-demo/situation-demo.component'
import { AuthGuardService } from '../core/auth-guard.service'
const routes: Routes = [
  {path: '', canActivate:[AuthGuardService], component: SituationComponent},
  {path: 'demo', canActivate:[AuthGuardService], component: SituationDemoComponent}
]

@NgModule({
  imports: [
    RouterModule.forChild(routes)
  ],
  exports: [RouterModule]
})
export class SituationRouterModule { }
