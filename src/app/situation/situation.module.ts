import { NgModule } from '@angular/core';
import { SharedModule } from '../shared/shared.module';
import { SituationRouterModule } from './situation-router.module'

import { SituationComponent } from './situation/situation.component';
import { SituationService } from './services/situation.service';
import { TotalRealtimePanelComponent } from './common/total-realtime-panel/total-realtime-panel.component';
import { GeogPanelComponent } from './common/geog-panel/geog-panel.component';
import { UserPanelComponent } from './common/user-panel/user-panel.component';
import { SinglePanelComponent } from './common/single-panel/single-panel.component';
import { PanelToggleWrapComponent } from './common/panel-toggle-wrap/panel-toggle-wrap.component';
import { SelectSinglePanelComponent } from './common/select-single-panel/select-single-panel.component';
import { SituationHeaderComponent } from './common/situation-header/situation-header.component';
import { SituationDemoComponent } from './situation-demo/situation-demo.component'

@NgModule({
  imports: [
    SharedModule,
    SituationRouterModule
  ],
  declarations: [SituationComponent, TotalRealtimePanelComponent, GeogPanelComponent, UserPanelComponent, SinglePanelComponent, PanelToggleWrapComponent, SelectSinglePanelComponent, SituationHeaderComponent, SituationDemoComponent],
  providers: [SituationService]
})
export class SituationModule { }
