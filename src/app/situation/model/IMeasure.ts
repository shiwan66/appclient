export interface IMeasure {
    key: string;
    value: string;
}

export interface  IMeasures {
    deviceId: string;
    measures: IMeasure[];
}