import { NgModule } from '@angular/core';
import { SharedModule } from '../shared/shared.module'
import { ErrorRouterModule } from './error-router.module'

import { ErrorService } from './services/error.service'

import { ErrorComponent } from './error/error.component';
import { Error404Component } from './common/error404/error404.component';
import { Error500Component } from './common/error500/error500.component';

@NgModule({
  imports: [
    SharedModule,
    ErrorRouterModule
  ],
  declarations: [ErrorComponent, Error404Component, Error500Component],
  providers: [ErrorService]
})
export class ErrorModule { }
