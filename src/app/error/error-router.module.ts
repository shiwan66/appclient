import { NgModule } from '@angular/core';
import { RouterModule, Routes } from "@angular/router";
import { ErrorComponent } from './error/error.component';
import { Error404Component } from './common/error404/error404.component';
import { Error500Component } from './common/error500/error500.component';

const routes: Routes = [
  {
  	path: '', component: ErrorComponent, children: [
  		{path: '404', component: Error404Component},
  		{path: '500', component: Error500Component}
  	]
  }
]

@NgModule({
  imports: [
    RouterModule.forChild(routes)
  ],
  exports: [RouterModule]
})
export class ErrorRouterModule { }
