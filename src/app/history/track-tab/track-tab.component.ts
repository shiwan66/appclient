import { Component, OnInit, Input } from '@angular/core';
import {
	MapOptions, NavigationControlOptions, OverviewMapControlOptions, ScaleControlOptions, MapTypeControlOptions,
	ControlAnchor, NavigationControlType, MapTypeControlType,
	MarkerOptions, Point
} from 'angular2-baidu-map';
import { HistoryService } from '../services/history.service'
declare var $: any;
declare var INFOBOX_AT_TOP: any;

@Component({
  selector: 'app-track-tab',
  templateUrl: './track-tab.component.html',
  styleUrls: ['./track-tab.component.scss']
})
export class TrackTabComponent implements OnInit {
	@Input() deviceId?: string;
  	tracklist: Array<any>;
	myMap: any;
	public opts: MapOptions;
	public controlOpts: NavigationControlOptions;
	public scaleOpts: ScaleControlOptions;
	public markers: Array<{ point: Point; options?: MarkerOptions }>
	public currentTrack: any;  // 仪器当前溯源信息
	public markerList: Array<any> = [];
	bsRangeValue: any=[];
	showMap: boolean = false;
	showInfoBox: any;
	line:any;
	linecheck:boolean = false;

	constructor(private historySvc: HistoryService) { }

	ngOnInit() {
		this.historySvc.getDeployByDate(this.deviceId).subscribe(result => {
			if(result) {
				this.tracklist = result.datas;
			}
		})
	}

	ngOnChanges(change) {
		// 当切换当前仪器时，进行更新refresh
		if(!change.deviceId.firstChange) {
			this.refresh();
		}
	}

	ngAfterViewInit() {	
		var inter = setInterval(() => {
			if(window.BMap) {
				$('body').append('<script src="/assets/plugin/InfoBox_min.js"></script>')
				clearInterval(inter);
			}
		}, 100)	
	}

	refresh() {
		// 数据清零
		this.line = null;
		this.linecheck = false;
		this.tracklist = [];
		this.markerList = [];
		this.markers = null;
		// 清除地图覆盖物
		if(this.myMap) this.myMap.clearOverlays();
		this.historySvc.getDeployByDate(this.deviceId, this.bsRangeValue[0], this.bsRangeValue[1]).subscribe(result => {
			if(result) {
				this.tracklist = result.datas;
				this.initMap();
			}
		})
	}
 
	// 初始化地图
	initMap() {
		if(!this.showMap) this.showMap = true;
		this.opts = {
			centerAndZoom: {
				lng: 114.314933,
				lat: 30.606137,
				zoom: 15
			},
			enableScrollWheelZoom: true
		};

		this.controlOpts = {
			anchor: ControlAnchor.BMAP_ANCHOR_TOP_LEFT,
			type: NavigationControlType.BMAP_NAVIGATION_CONTROL_LARGE,
			offset: { width: 50, height: 150 }
		}

		this.scaleOpts = {
			anchor: ControlAnchor.BMAP_ANCHOR_BOTTOM_LEFT
		}

		this.currentTrack = this.tracklist[0]
		this.addMarkers(this.tracklist);
	}

	// 添加marker
	addMarkers(points) {
		var list = points.map((item) => {
			return {
				point: {
					lng: <number>item.longitude,
					lat: <number>item.latitude
				}
			}
		})
		this.markers = list;
	}

	// info窗
	public showWindow({ e, marker, map }: any, index): void {
		var deviceInfo = this.tracklist[index];
		// map.openInfoWindow(
		// 	new window.BMap.InfoWindow(`
		// 			仪器编号：${deviceInfo.deviceId}</br>
		// 			仪器名称：${deviceInfo.deviceType}</br>
		// 			生产厂家：${deviceInfo.deviceProUnitName}</br>
		// 			所在位置：${deviceInfo.areaName}</br>
		// 			购买单位：${deviceInfo.deviceProUnitName}</br>
		// 			在线状态：${deviceInfo.onlineState}</br>
		// 			健康状态：${deviceInfo.healthState}</br>`, {
		// 			offset: new window.BMap.Size(0, -30),
		// 			title: '仪器信息'
		// 		}),
		// 	marker.getPosition()
		// );
		var content = `
		<div class="infowindow">
			<ul class="infoWindow-list">
				<li class="row no-gutters">
					<div class="col-md-4 text-right key">仪器编号：</div>
					<div class="col text-left value">${deviceInfo.deviceId}</div>
				</li>
				<li class="row no-gutters">
					<div class="col-md-4 text-right key">仪器类型：</div>
					<div class="col text-left value">${deviceInfo.deviceType}</div>
				</li>
				<li class="row no-gutters">
					<div class="col-md-4 text-right key">生产厂家：</div>
					<div class="col text-left value">青岛崂应</div>
				</li>
				<li class="row no-gutters">
					<div class="col-md-4 text-right key">所在位置：</div>
					<div class="col text-left value">${deviceInfo.positionName}</div>
				</li>
				<li class="row no-gutters">
					<div class="col-md-4 text-right key">购买单位：</div>
					<div class="col text-left value">${deviceInfo.devicePurUnitName}</div>
				</li>
				<li class="row no-gutters">
					<div class="col-md-4 text-right key">在线状态：</div>
					<div class="col text-left value">${deviceInfo.onlineState}</div>
				</li>
				<li class="row no-gutters">
					<div class="col-md-4 text-right key">健康状态：</div>
					<div class="col text-left value">${deviceInfo.healthState}</div>
				</li>
			</ul>						
		</div>`;
		var infoBox = new (<any>window.BMapLib).InfoBox(this.myMap, content, {
			offset:{height:0,width:0},
			boxStyle:{
				border: '1px solid #00a6fe',
				borderRadius: '12px',				
				background: 'rgba(12, 58, 104, 0.9)',
				width: '250px',
				height: '245px'
			},
			boxClass:"infobox",
			closeIconMargin: "5px 8px 0 0",
			closeIconUrl: "assets/image/close.png",
			enableAutoPan: true,
			align: INFOBOX_AT_TOP
		});
		if(this.showInfoBox && this.showInfoBox._isopen) {
			this.showInfoBox.close();
		}
		this.showInfoBox = infoBox;
		this.showInfoBox.open(marker)
		this.myMap.removeOverlay(marker.getIcon());
		this.selectDevice(deviceInfo.deviceId);
	}
	loadMarker(marker: any, index:number) {
		this.markerList.push(marker);
	}
	loadMap(map: any) {
		this.myMap = map;
	}

	toggleLine() {
		if(this.line) {
			this.myMap.removeOverlay(this.line);
			this.line = null;
		} else {
			this.line = new window.BMap.Polyline(this.markerList.map(item => item.getPosition()));
			this.myMap.addOverlay(this.line, {strokeColor: "red", strokeWeight: 5, strokeOpacity: 1});
		}
	}

	// 选择仪器
	public selectDevice(id?: number, index?: any) {
		if (id == null) {
			id = this.tracklist[index].deviceId
		}
		this.currentTrack = this.tracklist.filter(item => item && item.deviceId == id)[0];
		if (!this.currentTrack) return;
		let currentMarker = this.markerList.filter(item => {
			console.dir(item.getPosition());
			return item.getPosition().lng == this.currentTrack.longitude && item.getPosition().lat == this.currentTrack.latitude
		})[0];
		if (currentMarker != null) {
			this.myMap.panTo(currentMarker.getPosition())
		}
	}

}
