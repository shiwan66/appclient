import { async, ComponentFixture, TestBed } from '@angular/core/testing';

import { TrackTabComponent } from './track-tab.component';

describe('TrackTabComponent', () => {
  let component: TrackTabComponent;
  let fixture: ComponentFixture<TrackTabComponent>;

  beforeEach(async(() => {
    TestBed.configureTestingModule({
      declarations: [ TrackTabComponent ]
    })
    .compileComponents();
  }));

  beforeEach(() => {
    fixture = TestBed.createComponent(TrackTabComponent);
    component = fixture.componentInstance;
    fixture.detectChanges();
  });

  it('should create', () => {
    expect(component).toBeTruthy();
  });
});
