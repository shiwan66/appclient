import { NgModule } from '@angular/core';
import { RouterModule, Routes } from "@angular/router";
import { AuthGuardService } from '../core/auth-guard.service'
import { HistoryComponent } from './history/history.component';

const routes: Routes = [
  {path: '', canActivate:[AuthGuardService], component: HistoryComponent}
]

@NgModule({
  imports: [
    RouterModule.forChild(routes)
  ],
  exports: [RouterModule]
})
export class HistoryRouterModule { }
