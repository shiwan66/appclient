import { Injectable } from '@angular/core';
import { Observable } from 'rxjs';
import { Http, Headers, Response, RequestOptionsArgs } from '@angular/http';
import { Router } from '@angular/router';
import 'rxjs/add/operator/map';
@Injectable()
export class HistoryService {

  constructor(private http: Http, private router: Router) { }
  dateFormat = function (fmt, date) { //author: meizz 
    var o = {
        "M+": date.getMonth() + 1, //月份 
        "d+": date.getDate(), //日 
        "h+": date.getHours(), //小时 
        "m+": date.getMinutes(), //分 
        "s+": date.getSeconds(), //秒 
        "q+": Math.floor((date.getMonth() + 3) / 3), //季度 
        "S": date.getMilliseconds() //毫秒 
    };
    if (/(y+)/.test(fmt)) fmt = fmt.replace(RegExp.$1, (date.getFullYear() + "").substr(4 - RegExp.$1.length));
    for (var k in o)
    if (new RegExp("(" + k + ")").test(fmt)) fmt = fmt.replace(RegExp.$1, (RegExp.$1.length == 1) ? (o[k]) : (("00" + o[k]).substr(("" + o[k]).length)));
    return fmt;
}

  //获取仪器部署表
  getDeployList(page: number, pageSize: number,startTime?: Date, endTime?: Date, instrumentId?: string): Observable<any> {
    let url = `/a/history/deploylist/datas?page=${page}&pageSize=${pageSize}`;
    if(startTime && startTime instanceof Date) {
      startTime = this.dateFormat("yyyy-MM-dd hh:mm:ss", startTime);
      url += `&startTime=${startTime}`
    }
    if(endTime){ 
      endTime = this.dateFormat("yyyy-MM-dd hh:mm:ss", endTime);
      url += `&endTime=${endTime}`
    }
    if(instrumentId){ 
      url += `&instrumentId=${instrumentId}`
    }
    return this.http.get(url).map(result => {
      return result.json();
    })
  }  
  
  //获取仪器时间划分部署表
  getDeployByDate(instrumentId: string,startTime?: Date, endTime?: Date): Observable<any> {
    let url = `/a/history/deploylist/datas_someone_instrument?instrumentId=${instrumentId}`;
    if(startTime && startTime instanceof Date) {
      startTime = this.dateFormat("yyyy-MM-dd hh:mm:ss", startTime);
      url += `&startTime=${startTime}`
    }
    if(endTime){ 
      endTime = this.dateFormat("yyyy-MM-dd hh:mm:ss", endTime);
      url += `&endTime=${endTime}`
    }
    return this.http.get(url).map(result => {
      return result.json();
    })
  }

  

}
