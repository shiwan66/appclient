import { Component, OnInit, ViewChild,  } from '@angular/core';
import { TabsetComponent } from 'ngx-bootstrap';
import { HistoryService } from '../services/history.service'
import { ActivatedRoute, Params, Router } from '@angular/router';
declare var $: any;
import { Observable } from 'rxjs';
import 'rxjs/add/operator/switchMap';

@Component({
  selector: 'app-history',
  templateUrl: './history.component.html',
  styleUrls: ['./history.component.scss']
})
export class HistoryComponent implements OnInit {
  model:Object = {};
  @ViewChild('staticTabs') staticTabs: TabsetComponent;
  tab_id: number = 0;
  currentDev: any;
  instrumentId: string;

  constructor(private historySvc: HistoryService, 
    private route: ActivatedRoute,) { }

  ngOnInit() {
    $('.tab-content').addClass("h-100")
    this.route.queryParamMap
      .switchMap((params: Params) => {
        if (params.get('instrumentId')) {
          this.instrumentId = params.get('instrumentId');
        }
        return Observable.of<any>(null);
      }).subscribe(
        result => { }
      );
  }

  ngAfterViewInit() {
  }
  switchTab(tab_id: number) {
    if(this.tab_id != tab_id) {
      this.tab_id = tab_id;
      this.staticTabs.tabs[tab_id].active = true;
    }
  }
}
