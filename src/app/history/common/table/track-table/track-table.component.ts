import { Component, OnInit, Input, Output, EventEmitter, SimpleChanges, ChangeDetectionStrategy } from '@angular/core';

@Component({
  selector: 'app-track-table',
  templateUrl: './track-table.component.html',
  changeDetection: ChangeDetectionStrategy.OnPush,
  styleUrls: ['./track-table.component.scss']
})
export class TrackTableComponent implements OnInit {
  public rows:Array<any> = [];
  public columns:Array<any> = [
    {title: '任务号',      name: 'taskId'},
    {title: '仪器ID',      name: 'deviceId'},
    {title: '仪器类型',    name: 'deviceType'},
    {title: '测量批号',    name: 'batchNumber' },
    {title: '位置名称', name: 'positionName' },
    {title: '开始时间',    name: 'startTime'},
    {title: '结束时间',    name: 'endTime'}, 
    {title: 'O2浓度均值',  name: 'o2Averaged' } ,
    {title: 'CO浓度均值',  name: 'coAveraged' } , 
    {title: 'CO2浓度均值', name: 'co2Averaged'} , 
    {title: 'SO2浓度均值', name: 'so2Averaged'} , 
    {title: 'NO浓度均值',  name: 'noAveraged' }, 
    {title: 'NO2浓度均值', name: 'no2Averaged'}, 
    {title: 'NOx浓度均值', name: 'noxAveraged' }, 
    {title: 'H2S浓度均值', name: 'h2sveraged' }
  ];
  public page:number = 1;
  public itemsPerPage:number = 10;
  public maxSize:number = 5;
  public numPages:number = 1;
  public length:number = 0;

  public config:any = {
    paging: true,
    // sorting: {columns: this.columns},
    filtering: {filterString: ''},
    className: ['table-striped', 'table-bordered', 'font12']
  };
  public configs: any;

  @Input() data?:Array<any> = [];
  @Output() clickItem = new EventEmitter();
  firstText: string = "首页";
  lastText: string = "尾页";
  previousText: string = "上一页";
  nextText: string = "下一页";

  public constructor() {
  }

  public ngOnInit():void {
    this.length = this.data.length;
    this.onChangeTable(this.config);
  }

  ngOnChanges(changes: SimpleChanges) {
    this.onChangeTable(this.config);
  }


  public changePage(page:any, data:Array<any> = this.data):Array<any> {
    let start = (page.page - 1) * page.itemsPerPage;
    let end = page.itemsPerPage > -1 ? (start + page.itemsPerPage) : data.length;
    return data.slice(start, end);
  }

  public changeSort(data:any, config:any):any {
    if (!config.sorting) {
      return data;
    }

    let columns = this.config.sorting.columns || [];
    let columnName:string = void 0;
    let sort:string = void 0;

    for (let i = 0; i < columns.length; i++) {
      if (columns[i].sort !== '' && columns[i].sort !== false) {
        columnName = columns[i].name;
        sort = columns[i].sort;
      }
    }

    if (!columnName) {
      return data;
    }

    // simple sorting
    return data.sort((previous:any, current:any) => {
      if (previous[columnName] > current[columnName]) {
        return sort === 'desc' ? -1 : 1;
      } else if (previous[columnName] < current[columnName]) {
        return sort === 'asc' ? -1 : 1;
      }
      return 0;
    });
  }

  public changeFilter(data:any, config:any):any {
    let filteredData:Array<any> = data;
    this.columns.forEach((column:any) => {
      if (column.filtering) {
        filteredData = filteredData.filter((item:any) => {
          return item[column.name].match(column.filtering.filterString);
        });
      }
    });

    if (!config.filtering) {
      return filteredData;
    }

    if (config.filtering.columnName) {
      return filteredData.filter((item:any) =>
        item[config.filtering.columnName].match(this.config.filtering.filterString));
    }

    let tempArray:Array<any> = [];
    filteredData.forEach((item:any) => {
      let flag = false;
      this.columns.forEach((column:any) => {
        if (item[column.name].toString().match(this.config.filtering.filterString)) {
          flag = true;
        }
      });
      if (flag) {
        tempArray.push(item);
      }
    });
    filteredData = tempArray;

    return filteredData;
  }

  public onChangeTable(config:any, page:any = {page: this.page, itemsPerPage: this.itemsPerPage}):any {
    if(this.configs == null) this.configs = config;
    if (config.filtering) {
      Object.assign(this.config.filtering, config.filtering);
    }

    if (config.sorting) {
      Object.assign(this.config.sorting, config.sorting);
    }

    let filteredData = this.changeFilter(this.data, this.config);
    let sortedData = this.changeSort(filteredData, this.config);
    this.rows = page && config.paging ? this.changePage(page, sortedData) : sortedData;
    this.length = sortedData.length;
  }

  public onCellClick(data: any): any {
    console.log(data);
    this.clickItem.emit(data.row.deviceId);
  }

  public pageChanged(result) {
    this.onChangeTable(this.config, result)
  }
}
