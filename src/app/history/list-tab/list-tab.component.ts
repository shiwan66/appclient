import { Component, OnInit, Input, Output, EventEmitter } from '@angular/core';
import { HistoryService } from '../services/history.service'
import { DeviceService } from '../../device/services/device.service'
declare var $: any;
import { AuthService } from '../../core/auth.service';

@Component({
  selector: 'app-list-tab',
  templateUrl: './list-tab.component.html',
  styleUrls: ['./list-tab.component.scss']
})
export class ListTabComponent implements OnInit {
  public columns:Array<any> = [
    {title: '任务号',      name: 'taskId'},
    {title: '仪器ID',      name: 'deviceId'},
    {title: '仪器类型',    name: 'batchNumber'},
    {title: '测量批号',    name: 'deviceType' },
    {title: '位置名称',    name: 'positionName' },
    {title: '开始时间',    name: 'startTime'},
    {title: '结束时间',    name: 'endTime'}, 
    {title: 'O2浓度均值',  name: 'o2Averaged' } ,
    {title: 'CO浓度均值',  name: 'coAveraged' } , 
    {title: 'CO2浓度均值', name: 'co2Averaged'} , 
    {title: 'SO2浓度均值', name: 'so2Averaged'} , 
    {title: 'NO浓度均值',  name: 'noAveraged' }, 
    {title: 'NO2浓度均值', name: 'no2Averaged'}, 
    {title: 'NOx浓度均值', name: 'noxAveraged' }, 
    {title: 'H2S浓度均值', name: 'h2sveraged'}
  ];
  public config = {
    className: ['table-striped', 'table-bordered', "font12"]
  }
  pageSize:number = 10;
  bsRangeValue: any = [];
  data: Array<any>=[];
  count: number = 0;
  instrumentIdValue: any;
  // 双向数据绑定
  @Output() instrumentIdChange: EventEmitter<any> = new EventEmitter();
  @Input() 
  get instrumentId() {
    return this.instrumentIdValue;
  }
  set instrumentId(v) {
    this.instrumentIdValue = v;
    this.instrumentIdChange.emit(this.instrumentIdValue);
  }
  photos;

  public constructor(
    private historySvc: HistoryService, 
    private auth: AuthService, 
    private deviceSvc: DeviceService) {
  }

  public ngOnInit():void {
    this.refresh();
    this.deviceSvc.getInstruments("", 1, 10000).subscribe(result => {
      if(result) {
        this.photos = result.datas as any;
      }
    }, (err) => {
        this.auth.handleError(err);
      }
    )
  }

  cellClick(data) {
    this.instrumentId = this.data.filter(item => item.deviceId == data.row.deviceId)[0].deviceId;
    var result = data.row[data.column];
  }

	refresh() {
    this.data = null;
    this.historySvc.getDeployList(1, this.pageSize, this.bsRangeValue[0], this.bsRangeValue[1], this.instrumentId).subscribe(result => {
      if(result) {
        this.data = result.datas;
        this.count = result.count;
      }
    }, (err) => {
        this.auth.handleError(err);
      }
    )
  }
  
  pagingChange(result) {
    this.historySvc.getDeployList(result.page, result.pageSize, this.bsRangeValue[0], this.bsRangeValue[1], this.instrumentId).subscribe(result => {
      if(result) {
        this.data = result.datas;
        this.count = result.count;
      }
    }, (err) => {
      this.auth.handleError(err);
    });
  }
}
