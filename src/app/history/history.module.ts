import { NgModule } from '@angular/core';
import { SharedModule } from '../shared/shared.module'
import { HistoryRouterModule } from './history-router.module'
import { HistoryService } from './services/history.service'
import { DeviceService } from '../device/services/device.service'


import { HistoryComponent } from './history/history.component';
import { DeviceTableComponent } from './common/table/device-table/device-table.component';
import { TrackTableComponent } from './common/table/track-table/track-table.component';
import { TrackTabComponent } from './track-tab/track-tab.component';
import { MapTabComponent } from './map-tab/map-tab.component';
import { ListTabComponent } from './list-tab/list-tab.component';

@NgModule({
  imports: [
    SharedModule,
    HistoryRouterModule
  ],
  declarations: [HistoryComponent, DeviceTableComponent, TrackTableComponent, TrackTabComponent, MapTabComponent, ListTabComponent],
  providers: [HistoryService, DeviceService]
})
export class HistoryModule { }
