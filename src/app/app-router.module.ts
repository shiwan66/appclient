import { NgModule } from '@angular/core';
import { RouterModule, Routes } from "@angular/router";


const routes: Routes = [
  {path: '', redirectTo: 'login', pathMatch: 'full'},
  {path: 'login', loadChildren: 'app/login/login.module#LoginModule'},
  {path: 'test', loadChildren: 'app/test/test.module#TestModule'},
  {
    path: 'app', 
    children: [
      {path: 'home', loadChildren: 'app/home/home.module#HomeModule'},
      {path: 'situation', loadChildren: 'app/situation/situation.module#SituationModule'},
      {path: 'history', loadChildren: 'app/history/history.module#HistoryModule'},
      {path: 'warning', loadChildren: 'app/warning/warning.module#WarningModule'},
      {path: 'customer', loadChildren: 'app/customer-serve/customer-serve.module#CustomerServeModule'},
      {path: 'report', loadChildren: 'app/report/report.module#ReportModule'},
      {path: 'state', loadChildren: 'app/sys-state/sys-state.module#SysStateModule'},
      {path: 'error', loadChildren: 'app/error/error.module#ErrorModule'},
      {path: 'device', loadChildren: 'app/device/device.module#DeviceModule'},
      {path: 'step', loadChildren: 'app/step/step.module#StepModule'},
    ]
  }
];

@NgModule({
  imports: [
    RouterModule.forRoot(routes)
  ],
  exports: [RouterModule]
})
export class AppRouterModule { }
