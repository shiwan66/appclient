import { NgModule } from '@angular/core';
import { SharedModule } from '../shared/shared.module';
import { HomeRouterModule } from './home-router.module'
import { CarouselModule } from 'ngx-bootstrap'

import { HomeComponent } from './home/home.component';
import { MenuItemComponent } from './menu-item/menu-item.component';

@NgModule({
  imports: [
    SharedModule,
    HomeRouterModule,
    CarouselModule
  ],
  declarations: [HomeComponent, MenuItemComponent]
})
export class HomeModule { }
