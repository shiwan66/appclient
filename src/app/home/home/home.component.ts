import { Component, OnInit } from '@angular/core';

@Component({
  selector: 'app-home',
  templateUrl: './home.component.html',
  styleUrls: ['./home.component.scss']
})
export class HomeComponent implements OnInit {
  menulist = [
    {
      imgUrl: "/assets/img/sbztts.svg",
      url: '/app/situation',
      title: "仪器整体态势",
      info: "整体展示仪器在线工作情况"
    },
    {
      imgUrl: "/assets/img/sbycjk.svg",
      href: 'http://47.92.28.186:8080/hnty/a/monitor/device/view',
      title: "测量过程监控",
      info: "工步测量数据展示"
    },
    {
      imgUrl: "/assets/img/lssjcx1.svg",
      url: '/app/history',
      title: "测量历史溯源",
      info: "查询仪器历史工作数据"
    },
    {
      imgUrl: "/assets/img/tzybj.svg",
      url: '/app/warning',
      title: "测量通知报警",
      info: "仪器报警与通知信息处理报警信息总数：100条"
    },
    {
      imgUrl: "/assets/img/bbdy.svg",
      url: '/app/report',
      title: "仪器测量报表",
      info: "仪器测量数据打印"
    },
    {
      imgUrl: "/assets/img/xsjfwwd.svg",
      url: '/app/customer',
      title: "销售及服务网点",
      info: "汇总全国销售与服务网点已有网点数：40个"
    },
    {
      imgUrl: "/assets/img/sbgl.svg",
      url: '/app/device',
      title: "仪器运维管理",
      info: "仪器运营管理与维护"
    },
    {
      imgUrl: "/assets/img/xtyxzt.svg",
      url: '/app/state',
      title: "系统运行状态",
      info: "仪器管理平台运营监控"
    }
  ]
  constructor() { }

  ngOnInit() {
  }

}
