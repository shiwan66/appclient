import { NgModule } from '@angular/core';
import { RouterModule, Routes } from "@angular/router";
import { CustomerServeComponent } from './customer-serve/customer-serve.component'

const routes: Routes = [
  {path: '', component: CustomerServeComponent}
]
@NgModule({
  imports: [
    RouterModule.forChild(routes)
  ],
  exports: [RouterModule]
})
export class CustomerServeRouterModule { }
