import { async, ComponentFixture, TestBed } from '@angular/core/testing';

import { CustomerServeComponent } from './customer-serve.component';

describe('CustomerServeComponent', () => {
  let component: CustomerServeComponent;
  let fixture: ComponentFixture<CustomerServeComponent>;

  beforeEach(async(() => {
    TestBed.configureTestingModule({
      declarations: [ CustomerServeComponent ]
    })
    .compileComponents();
  }));

  beforeEach(() => {
    fixture = TestBed.createComponent(CustomerServeComponent);
    component = fixture.componentInstance;
    fixture.detectChanges();
  });

  it('should create', () => {
    expect(component).toBeTruthy();
  });
});
