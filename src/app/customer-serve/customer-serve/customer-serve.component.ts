import { Component, OnInit } from '@angular/core';
import {
  MapOptions, NavigationControlOptions, OverviewMapControlOptions, ScaleControlOptions, MapTypeControlOptions,
  ControlAnchor, NavigationControlType, MapTypeControlType,
  MarkerOptions, Point
} from 'angular2-baidu-map';
import { CustomerServeService } from '../services/customer-serve.service'
declare var $: any;
declare var BMAPLIB_TAB_SEARCH:any;
declare var BMAPLIB_TAB_TO_HERE:any;
declare var BMAPLIB_TAB_FROM_HERE:any;

@Component({
  selector: 'app-customer-serve',
  templateUrl: './customer-serve.component.html',
  styleUrls: ['./customer-serve.component.scss']
})
export class CustomerServeComponent implements OnInit {

  myMap: any;
  public opts: MapOptions;
  public controlOpts: NavigationControlOptions;
  public overviewmapOpts: OverviewMapControlOptions;
  public scaleOpts: ScaleControlOptions;
  public mapTypeOpts: MapTypeControlOptions;
  public markers: Array<{ point: Point; options?: MarkerOptions }>
  public serveList: Array<any>;
  public currentServe: any;
  public markerList: Array<any> = [];
  currentSearchInfoWindow: any;
  constructor(private customerSvc: CustomerServeService) { }

  ngOnInit() {
    this.customerSvc.getServeList().subscribe(result => {
      if (result) {
        this.initMap(result)
      }
    })
  }

	ngAfterViewInit() {		
		setTimeout(() => { $('body').append('<script type="text/javascript" src="http://api.map.baidu.com/library/SearchInfoWindow/1.5/src/SearchInfoWindow_min.js"></script><link rel="stylesheet" href="http://api.map.baidu.com/library/SearchInfoWindow/1.5/src/SearchInfoWindow_min.css" />') }, 1000)
  }
   
  // 初始化地图
  initMap(result: any) {
    this.opts = {
      centerAndZoom: {
        lng: 117.126399,
        lat: 36.656554,
        zoom: 6
      },
      enableScrollWheelZoom: true
    };

    this.controlOpts = {
      anchor: ControlAnchor.BMAP_ANCHOR_TOP_LEFT,
      type: NavigationControlType.BMAP_NAVIGATION_CONTROL_LARGE,
      offset: { width: 50, height: 150 }
    }

    this.overviewmapOpts = {
      anchor: ControlAnchor.BMAP_ANCHOR_BOTTOM_RIGHT,
      isOpen: true
    }

    this.scaleOpts = {
      anchor: ControlAnchor.BMAP_ANCHOR_BOTTOM_LEFT
    }

    this.mapTypeOpts = {
      type: MapTypeControlType.BMAP_MAPTYPE_CONTROL_HORIZONTAL
    }

    this.serveList = result.datas;
    this.currentServe = this.serveList[0]
    this.addMarkers(this.serveList);
  }


  // 添加marker
  addMarkers(points) {
    var list = points.map((item) => {
      return <{ point: Point; options?: MarkerOptions }>{
        point: {
          lng: <number>item.longitude,
          lat: <number>item.latitude
        }
      }
    })
    this.markers = list;
  }


  // info窗
  public showWindow({ e, marker, map }: any, index): void {
    var serveInfo = this.serveList[index];
    //定义检索信息窗口样式
    var winStyle = {
      title: "<p style='margin:8px;line-height:1;font-size:13px'><b>售后维修网点</b></p>",	//标题
      width: 450,												//宽度
      height: 130,											//高度
      enableSendToPhone: false,					//隐藏发送短信按钮
      panel: "panelSearch",							//检索结果面板
      enableAutoPan: true,							//自动平移
      searchTypes: [
          BMAPLIB_TAB_SEARCH,						//周边检索
          BMAPLIB_TAB_TO_HERE,					//到这里去
          BMAPLIB_TAB_FROM_HERE					//从这里出发
      ]
    }
    var tmptxt =  `
        <div class="d-flex">
          <div class="col" style="margin:0;line-height:20px;padding:2px;">
            名称：${serveInfo.name}
            <br/>
            ${serveInfo.contactInfo.replace(/;/g, "<br/>")}
            <br/>
            地址：${serveInfo.address}
          </div>
          <div class=col-auto">
            <img src="/assets/CustomerService/image/${serveInfo.photoName}" alt="" style="float:right;zoom:1;overflow:hidden;width:130px;height:160px;margin-left:3px;"/>
          </div>
        </div>
        `   																													
    var searchInfoWindow = new (<any>window.BMapLib).SearchInfoWindow(map, tmptxt, winStyle);			//创建带检索功能的信息窗	    
    searchInfoWindow.open(marker);
  }
  loadMarker(marker: any) {
    var servInfo = this.serveList.filter(item => item.longitude == marker.getPosition().lng && item.latitude == marker.getPosition().lat)[0];
    var label = new window.BMap.Label(servInfo.name, <any>{ offset: new window.BMap.Size(20, -10) });
    marker.setLabel(label);
    this.markerList.push(marker);
  }
  loadMap(map: any) {
    this.myMap = map;
  }
}
