import { NgModule } from '@angular/core';
import { SharedModule } from '../shared/shared.module';
import { CustomerServeRouterModule } from './customer-serve-router.module'

import { CustomerServeService } from './services/customer-serve.service'

import { CustomerServeComponent } from './customer-serve/customer-serve.component';

@NgModule({
  imports: [
    SharedModule,
    CustomerServeRouterModule
  ],
  declarations: [CustomerServeComponent],
  providers: [CustomerServeService]
})
export class CustomerServeModule { }
