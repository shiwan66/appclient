import { TestBed, inject } from '@angular/core/testing';

import { CustomerServeService } from './customer-serve.service';

describe('CustomerServeService', () => {
  beforeEach(() => {
    TestBed.configureTestingModule({
      providers: [CustomerServeService]
    });
  });

  it('should be created', inject([CustomerServeService], (service: CustomerServeService) => {
    expect(service).toBeTruthy();
  }));
});
