import { Injectable } from '@angular/core';
import { Router } from '@angular/router'
import {HttpErrorResponse} from '@angular/common/http';

@Injectable()
export class AuthService {
  constructor(private router: Router,) {}
  isLoggedIn = false;

  // store the URL so we can redirect after logging in
  redirectUrl: string;

  login(): boolean {
    return this.isLoggedIn;
  }

  logout(): void {
    this.isLoggedIn = false;
  }

  handleError(err: HttpErrorResponse) {
    localStorage.setItem("errorMsg", JSON.stringify((<any>err)._body))
    switch(err.status) {
      case 500:
        this.router.navigate(['/app/error/500'])
        break;
      case 404:
          this.router.navigate(['/app/error/404'])
          break;
      default : 
        console.dir(err);
        break;
    }
  }
  dateFormat = function (fmt, date) { //author: meizz 
    var o = {
        "M+": date.getMonth() + 1, //月份 
        "d+": date.getDate(), //日 
        "h+": date.getHours(), //小时 
        "m+": date.getMinutes(), //分 
        "s+": date.getSeconds(), //秒 
        "q+": Math.floor((date.getMonth() + 3) / 3), //季度 
        "S": date.getMilliseconds() //毫秒 
    };
    if (/(y+)/.test(fmt)) fmt = fmt.replace(RegExp.$1, (date.getFullYear() + "").substr(4 - RegExp.$1.length));
    for (var k in o)
    if (new RegExp("(" + k + ")").test(fmt)) fmt = fmt.replace(RegExp.$1, (RegExp.$1.length == 1) ? (o[k]) : (("00" + o[k]).substr(("" + o[k]).length)));
    return fmt;
  }

}
