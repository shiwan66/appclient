import { async, ComponentFixture, TestBed } from '@angular/core/testing';

import { StepStepsComponent } from './step-steps.component';

describe('StepStepsComponent', () => {
  let component: StepStepsComponent;
  let fixture: ComponentFixture<StepStepsComponent>;

  beforeEach(async(() => {
    TestBed.configureTestingModule({
      declarations: [ StepStepsComponent ]
    })
    .compileComponents();
  }));

  beforeEach(() => {
    fixture = TestBed.createComponent(StepStepsComponent);
    component = fixture.componentInstance;
    fixture.detectChanges();
  });

  it('should create', () => {
    expect(component).toBeTruthy();
  });
});
