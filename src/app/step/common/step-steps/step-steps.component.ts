import { Component, OnInit, TemplateRef, ViewChild, Input, Output, EventEmitter } from '@angular/core';
import { BsModalService } from 'ngx-bootstrap/modal';
import { BsModalRef } from 'ngx-bootstrap/modal/bs-modal-ref.service';
import { StepperOptions, NgxStepperComponent } from 'ngx-stepper';

import * as moment from 'moment';
import { Moment } from 'moment';
import { IMonthCalendarConfig, IDatePickerConfig, ECalendarValue, CalendarValue } from 'ng2-date-picker';

@Component({
  selector: 'app-step-steps',
  templateUrl: './step-steps.component.html',
  styleUrls: ['./step-steps.component.scss']
})
export class StepStepsComponent implements OnInit {
  @Input() instrumentId: string;
  @Input() instruList: any[];
  @Input() pageSize: number = 10;
  @Input() count: number = 0;
  @Output() refreshChange: EventEmitter<any> = new EventEmitter();
  displayDate: Moment | string;
  validationMinTime: Moment;
  startTime: string;
  validationMaxTime: Moment;
  endTime: string;
  config: IDatePickerConfig = {
    firstDayOfWeek: 'su',
    monthFormat: 'MMM, YYYY',
    disableKeypress: false,
    allowMultiSelect: false,
    closeOnSelect: undefined,
    closeOnSelectDelay: 100,
    openOnFocus: true,
    openOnClick: true,
    onOpenDelay: 0,
    weekDayFormat: 'ddd',
    appendTo: document.body,
    showNearMonthDays: true,
    showWeekNumbers: false,
    enableMonthSelector: true,
    yearFormat: 'YYYY',
    showGoToCurrent: true,
    dayBtnFormat: 'DD',
    monthBtnFormat: 'MMM',
    hours12Format: 'hh',
    hours24Format: 'HH',
    meridiemFormat: 'A',
    minutesFormat: 'mm',
    minutesInterval: 1,
    secondsFormat: 'ss',
    secondsInterval: 1,
    showSeconds: false,
    showTwentyFourHours: false,
    timeSeparator: ':',
    multipleYearsNavigateBy: 10,
    showMultipleYearsNavigation: false,
    locale: 'zh-cn',
    hideInputContainer: false,
    returnedValueType: ECalendarValue.Moment
  };
  invalid: boolean = false;
  message: string = "";

  @ViewChild('template')
  template: TemplateRef<any>;

  // 搜索模式
  isTime: boolean = true;
  isInstr: boolean = false;

  modalRef: BsModalRef;

  configs = {
    backdrop: true,
    ignoreBackdropClick: true
  };

  @ViewChild('stepperDemo')
  public steppers: NgxStepperComponent;
  public options: StepperOptions = {
    vertical: false,
    linear: true,
    alternative: true,
    mobileStepText: true,
    enableSvgIcon: true
  };

  constructor(private modalService: BsModalService) { }

  openModal(template: TemplateRef<any>) {
    this.modalRef = this.modalService.show(template, this.configs);
  }

  showModel() {
    this.openModal(this.template);
  }

  ngOnInit() {
    this.openModal(this.template);
  }

  // 搜索后刷新列表
  refresh() {
    this.modalRef.hide();
    // 触发搜索
    this.refreshChange.emit({
      startTime: this.startTime,
      endTime: this.endTime,
      instrumentId: this.instrumentId,
      pageSize: this.pageSize
    })
  }

  log(item: CalendarValue) {
    // if((<Moment>this.validationMaxTime).isBefore(this.validationMinTime)) this.invalid = true;
  }

  // 校验时间
  checkTime() {
    if (this.validationMaxTime) {
      this.invalid = (<Moment>this.validationMaxTime).isAfter(this.validationMinTime)
      if (this.invalid) {
        this.message = null;
        this.initTime();
      } else {
        this.message = "结束时间不能早于开始时间";
      }
    }
  }

  // 格式化时间
  initTime() {
    this.startTime = `${moment(this.displayDate).format("YYYY-MM-DD")} ${moment(this.validationMinTime).format("HH:mm:ss")}`
    this.endTime = `${moment(this.displayDate).format("YYYY-MM-DD")} ${moment(this.validationMaxTime).format("HH:mm:ss")}`
  }

  toggleTimeType() {
    if (this.isTime) this.isInstr = false;
    else this.isInstr = true;
  }
  toggleInstrType() {
    if (this.isInstr) this.isTime = false;
    else this.isTime = true;
  }
}
