import { Component, OnInit, TemplateRef, ViewChild, Input, Output, EventEmitter } from '@angular/core';
import { BsModalService } from 'ngx-bootstrap/modal';
import { BsModalRef } from 'ngx-bootstrap/modal/bs-modal-ref.service';
import { StepService } from '../../services/step.service'
import {DeviceService} from '../../../device/services/device.service'

import * as moment from 'moment';
import { Moment } from 'moment';
import { IMonthCalendarConfig, IDatePickerConfig, ECalendarValue, CalendarValue } from 'ng2-date-picker';

@Component({
  selector: 'app-setp-dialog',
  templateUrl: './setp-dialog.component.html',
  styleUrls: ['./setp-dialog.component.scss'],
  providers: [DeviceService]
})
export class SetpDialogComponent implements OnInit {
  // 当前仪器信息
  @Input() infoObj= null;
  
  @Input() viewStatus:boolean = false;
  @Input() instrumentId: string;
  instruList: any[];
  @Input() pageSize: number = 10;
  count: number = 0;
  @Output() refreshChange: EventEmitter<any> = new EventEmitter();
  // displayDate: Moment | string;
  // validationMinTime: Moment;
  startTime: string;
  // validationMaxTime: Moment;
  endTime: string;
  config: IDatePickerConfig = {
    firstDayOfWeek: 'su',
    monthFormat: 'MMM, YYYY',
    disableKeypress: false,
    allowMultiSelect: false,
    closeOnSelect: undefined,
    closeOnSelectDelay: 100,
    openOnFocus: true,
    openOnClick: true,
    onOpenDelay: 0,
    weekDayFormat: 'ddd',
    appendTo: document.body,
    showNearMonthDays: true,
    showWeekNumbers: false,
    enableMonthSelector: true,
    yearFormat: 'YYYY',
    showGoToCurrent: true,
    dayBtnFormat: 'DD',
    monthBtnFormat: 'MMM',
    hours12Format: 'hh',
    hours24Format: 'HH',
    meridiemFormat: 'A',
    minutesFormat: 'mm',
    minutesInterval: 1,
    secondsFormat: 'ss',
    secondsInterval: 1,
    showSeconds: false,
    showTwentyFourHours: false,
    timeSeparator: ':',
    multipleYearsNavigateBy: 10,
    showMultipleYearsNavigation: false,
    locale: 'zh-cn',
    hideInputContainer: false,
    returnedValueType: ECalendarValue.Moment
  };
  invalid: boolean = false;
  message: string = "";

  @ViewChild('template')
  template: TemplateRef<any>;

  modalRef: BsModalRef;
  configs = {
    backdrop: true,
    ignoreBackdropClick: true,
    class: 'modal-lg w80'
  };

  //calendar 选中日期
  selectDate: Date = new Date();
  //dateIsBefore 日期筛选在前
  dateIsBefore:boolean = false;

  constructor(
    private modalService: BsModalService,
    private stepSvc: StepService,
    private deviceSvc: DeviceService
  ) { }

  // 切换检索方式
  switchSearch() {
    this.dateIsBefore = !this.dateIsBefore;
    this.selectDate = new Date();
    this.instrumentId = null;
    if(this.dateIsBefore) {
      this.initTime();
      this.deviceSvc.getInstruments("",1, 20000 ).subscribe(result => {
        if(result) {
          this.instruList = result.datas;
          // this.count = result.count;
          this.selectDate = null;
        }
      })
    }
  }

  //选择仪器，进行相关处理


  openModal(template: TemplateRef<any>) {
    this.modalRef = this.modalService.show(template, this.configs);
  }

  showModel() {
    this.openModal(this.template);
  }

  ngOnInit() {
    if(!sessionStorage.getItem("selectStepList")) {
    this.openModal(this.template);
    }
  }

  // 搜索后刷新列表
  refresh() {
    this.initTime();
    this.modalRef.hide();
    // 触发搜索
    this.refreshChange.emit({
      startTime: this.startTime,
      endTime: this.endTime,
      instrumentId: this.instrumentId,
      pageSize: this.pageSize
    })
  }

  log(item: CalendarValue) {
    // if(item) {
    //   this.initTime();
    //   this.stepSvc.getInstrListByDate(this.startTime).subscribe(result => {
    //     if(result) {
    //       this.instruList = result.datas;
    //       this.count = result.count;
    //       if(this.instruList.findIndex(item => item.deviceId == this.instrumentId) < 0) {
    //         this.instrumentId = null;
    //       }
    //     }
    //   })
    // }
  }

  // 跳转图像化显示工步
  view() {
    const selectList = JSON.parse(sessionStorage.getItem("selectStepList"));
    const selectStepParams = JSON.parse(sessionStorage.getItem("selectStepParams"));
    const dateStr = selectStepParams.startTime.split(" ")[0].replace(/\-/g, "");
    if(selectList) {
      window.location.href= `http://47.92.28.186:8080/hnty/a/monitor/device/view?id=${this.instrumentId}&startId=${selectList[0].id}&endId=${selectList[selectList.length-1].id}&date=${dateStr}`
    }
  }

  // 格式化时间
  initTime() {
    this.startTime = `${moment(this.selectDate).format("YYYY-MM-DD")} 00:00:00}`
    this.endTime = `${moment(this.selectDate).format("YYYY-MM-DD")} 23:59:59}`
  }
  
  // 改变设备列表
  changeDeviceList(deviceList) {
    this.instruList = deviceList;
    this.instrumentId = null;
    this.initTime();
  }
}
