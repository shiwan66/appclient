import { async, ComponentFixture, TestBed } from '@angular/core/testing';

import { SetpDialogComponent } from './setp-dialog.component';

describe('SetpDialogComponent', () => {
  let component: SetpDialogComponent;
  let fixture: ComponentFixture<SetpDialogComponent>;

  beforeEach(async(() => {
    TestBed.configureTestingModule({
      declarations: [ SetpDialogComponent ]
    })
    .compileComponents();
  }));

  beforeEach(() => {
    fixture = TestBed.createComponent(SetpDialogComponent);
    component = fixture.componentInstance;
    fixture.detectChanges();
  });

  it('should create', () => {
    expect(component).toBeTruthy();
  });
});
