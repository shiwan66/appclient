import { Component, OnInit, Input, Output, EventEmitter, SimpleChanges } from '@angular/core';
import { StepService } from '../../services/step.service'

@Component({
  selector: 'app-calendar-panel',
  templateUrl: './calendar-panel.component.html',
  styleUrls: ['./calendar-panel.component.scss']
})
export class CalendarPanelComponent implements OnInit {
  @Input() dateIsBefore: boolean = false;
  view: string = 'month';
  @Input() deviceId?:string = "";

  // 当前月份
  viewDate: Date = new Date();
  // 当前天
  viewDay: number;
  locale: string = 'zh';

  clickedDateValue:Date = new Date();
  @Input() 
  get clickedDate() {
    return this.clickedDateValue;
  }
  set clickedDate(date) {
    this.clickedDateValue = date;
    this.clickedDateChange.emit(this.clickedDateValue);
  }
  @Output() 
  clickedDateChange:EventEmitter<Date> = new EventEmitter();
  @Output() 
  deviceListChange: EventEmitter<any> = new EventEmitter();

  datePanelResource: any[];
	//load 加载	
  public loading = false;

  constructor(
    private stepSvc:StepService
  ) { 
  }

  ngOnChanges(changes: SimpleChanges) {
    if(this.dateIsBefore && changes.deviceId) {
      this.monthChange(this.viewDate,this.deviceId);
    }
  }

  ngOnInit() {
    if(!this.dateIsBefore) {
      this.monthChange(this.viewDate);
    } 
  }

  // 选择月份
  monthChange(date: Date, deviceId?:string) {
    var year = date.getFullYear();
    var month = date.getMonth()+1;
    var result;
    if(month <10) result = `${year}0${month}`;
    else result = `${year}${month}`;
    this.loading = true;
    if(deviceId) {
      this.stepSvc.getWorkStepInfoListByInstruDate(result,deviceId).subscribe(results => {
        this.datePanelResource = results;
        this.loading = false;
      })
    } else if(!deviceId && this.deviceId) {
      this.stepSvc.getWorkStepInfoListByInstruDate(result,this.deviceId).subscribe(results => {
        this.datePanelResource = results;
        this.loading = false;
      })
    } else {
      this.stepSvc.getWorkStepInstrumentInfoListByDate(result).subscribe(results => {
        this.datePanelResource = results;
        this.loading = false;
      })
    }
  }

}
