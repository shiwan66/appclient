import { async, ComponentFixture, TestBed } from '@angular/core/testing';

import { CalendarPanelContentComponent } from './calendar-panel-content.component';

describe('CalendarPanelContentComponent', () => {
  let component: CalendarPanelContentComponent;
  let fixture: ComponentFixture<CalendarPanelContentComponent>;

  beforeEach(async(() => {
    TestBed.configureTestingModule({
      declarations: [ CalendarPanelContentComponent ]
    })
    .compileComponents();
  }));

  beforeEach(() => {
    fixture = TestBed.createComponent(CalendarPanelContentComponent);
    component = fixture.componentInstance;
    fixture.detectChanges();
  });

  it('should create', () => {
    expect(component).toBeTruthy();
  });
});
