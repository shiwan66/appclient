import { Component, OnInit, ChangeDetectionStrategy, Input, Output, EventEmitter } from '@angular/core';
import {
  CalendarEvent,
  CalendarDateFormatter,
  DAYS_OF_WEEK
} from 'angular-calendar';
import { CustomDateFormatter } from '../calendar-panel/custom-date-formatter.provider';
import { StepService } from '../../services/step.service'

@Component({
  selector: 'app-calendar-panel-content',
  templateUrl: './calendar-panel-content.component.html',
  styleUrls: ['./calendar-panel-content.component.scss'],
  changeDetection: ChangeDetectionStrategy.OnPush,
  providers: [
    {
      provide: CalendarDateFormatter,
      useClass: CustomDateFormatter
    }
  ]
})
export class CalendarPanelContentComponent implements OnInit {
  // 默认先选择时间
  @Input() dateIsBefore: boolean = false;
  @Input() view: string = 'month';

  // 当前月份
  @Input() viewDate: Date = new Date();
  // 当前天
  viewDay: number;

  events: CalendarEvent[] = [];

  @Input() locale: string = 'zh';

  weekStartsOn: number = DAYS_OF_WEEK.MONDAY;

  weekendDays: number[] = [DAYS_OF_WEEK.FRIDAY, DAYS_OF_WEEK.SATURDAY];

  oldDate: Date;
  clickedDateValue:Date = new Date();
  @Input() 
  get clickedDate() {
    return this.clickedDateValue;
  }
  set clickedDate(date) {
    this.clickedDateValue = date;
    this.oldDate = this.clickedDateValue;
    this.clickedDateChange.emit(this.clickedDateValue);
  }
  @Output() 
  clickedDateChange:EventEmitter<Date> = new EventEmitter();
  @Output() 
  deviceListChange: EventEmitter<any> = new EventEmitter();
  outNumber(date:Date): number {
    if(date != this.oldDate) {
      if(!this.dateIsBefore) 
        return this.datePanelResource[date.getDate()-1].length;
      else 
        return this.datePanelResource[date.getDate()-1];
    }
  }

  @Input() datePanelResource: any[];

  constructor(
    private stepSvc:StepService
  ) { 
  }

  ngOnInit() {}

  // 选择日期
  clickDate(date: any) {
    this.clickedDate = date.day.date;
    this.viewDay = this.clickedDate.getDate()-1;
    if(!this.dateIsBefore) {
      this.deviceListChange.emit(this.datePanelResource[this.viewDay]);
    }
  }

}
