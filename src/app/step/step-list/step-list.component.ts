import { Component, OnInit, TemplateRef } from '@angular/core';
import { ActivatedRoute, Params, Router } from '@angular/router';
import { AuthService } from '../../core/auth.service'
import { StepService } from '../services/step.service'
declare var $: any;
import { Observable } from 'rxjs';
import 'rxjs/add/operator/switchMap';

import { BsModalService } from 'ngx-bootstrap/modal';
import { BsModalRef } from 'ngx-bootstrap/modal/bs-modal-ref.service';

@Component({
  selector: 'app-step-list',
  templateUrl: './step-list.component.html',
  styleUrls: ['./step-list.component.scss']
})
export class StepListComponent implements OnInit {
  instrumentId: string;
  pageSize:number = 10;
  
  // count: number = 0;
  // instruList;
  viewStatus:boolean = false;

  rows = [];
  selected = [];
  
  startTime: string;
  endTime: string;

  modalRef: BsModalRef;
  initialState = {
    list: [],
    title: ''
  };

  // 当前仪器信息
  infoObj = null;
  constructor(
    private route: ActivatedRoute,
    public auth: AuthService,
    private stepSvc: StepService,
    private modalService: BsModalService
  ) { }
 
  openModal(template: TemplateRef<any>, value) {
    this.initialState = {
      list: value,
      title: '测量结果'
    }
    this.modalRef = this.modalService.show(template);
  }

  // 获取请求参数，初始化页面
  ngOnInit() {
    this.route.queryParamMap
      .switchMap((params: Params) => {
        if (params.get('instrumentId')) {
          this.instrumentId = params.get('instrumentId');
        }
        return Observable.of<any>(null);
      }).subscribe(
        result => { 
          var selectStepParamsStr = sessionStorage.getItem("selectStepParams");
          var selectStepListStr = sessionStorage.getItem("selectStepList");
          if(selectStepParamsStr) {
            var selectStepParams = JSON.parse(selectStepParamsStr);
            this.instrumentId = selectStepParams.instrumentId;
            this.startTime = selectStepParams.startTime;
            this.endTime = selectStepParams.endTime;
            this.getData(() => {
              if(selectStepListStr) {
                var selectStepList = JSON.parse(selectStepListStr);
                this.onSelect({selected:[selectStepList[0], selectStepList[selectStepList.length-1]]})       
              }
            });
          }
        }
      );
  }

  // 搜索后刷新列表
	refresh(result) {
    this.startTime = result.startTime;
    this.endTime = result.endTime;
    this.instrumentId = result.instrumentId;
    this.pageSize = result.pageSize;
    this.getData();
  }

  // 选中复选框
  onSelect({ selected }) {
    if(selected.length < 2) {
      this.selected.splice(0, this.selected.length);
      this.selected.push(...selected);
    } else if(selected.length == 2) {
      let index1 = this.rows.findIndex((value, index) =>  value.id == selected[0].id);
      let index2 = this.rows.findIndex((value, index) =>  value.id == selected[1].id)
      if(index1 > index2) {
        let tmp = index1;
        index1 = index2;
        index2 = tmp;
      }
      selected = this.rows.slice(index1, index2+1);
      this.selected = selected;
      sessionStorage.setItem("selectStepParams", JSON.stringify({
        instrumentId: this.instrumentId,
        startTime: this.startTime,
        endTime: this.endTime,
        pageSize: this.pageSize
      }));      
      sessionStorage.setItem("selectStepList", JSON.stringify(this.selected));
      this.viewStatus = true;
    } else {
      selected.splice(0, selected.length-1);
      this.selected = selected;
    }
  }


  // 获取工步列表数据
  getData(callback?:any) {
    this.infoObj = {
      instID:this.instrumentId,
      // instModel:this.rows[0].instModel,
      date:this.startTime.split(" ")[0]
    }
    this.stepSvc.getStepDatas(this.instrumentId, this.startTime, this.endTime).subscribe(result => {
      this.rows = result.datas;
      if(callback) callback();
    }, err => this.auth.handleError(err))
  }
  

}
