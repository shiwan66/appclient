import { Injectable } from '@angular/core';
import { Http, Headers, Response, RequestOptionsArgs } from '@angular/http';
import { Observable } from 'rxjs';
import { tap, delay } from 'rxjs/operators';
import 'rxjs/add/operator/map';
@Injectable()
export class StepService {

  constructor( private http: Http) { }

  // 获取请求列表(其实是设备列表)
  getInstrumentList(instrumentId?: string, startTime?: string, endTime?: string): Observable<any> {
    let url = `/a/history/deploylist/instrumentIdList`;
    if(instrumentId) url += `?instrumentId=${instrumentId}`;
    if(instrumentId && startTime) url += `&startTime=${startTime}`
    else if(!instrumentId && startTime) url += `?startTime=${startTime}`;
    if(endTime) url += `&endTime=${endTime}`;
    return this.http.get(url).map(result => {
      return result.json();
    })
  }

  // 获取工步历史列表
  getStepDatas(instrumentId?: string, startTime?: string, endTime?: string): Observable<any> {
    let url = `/a/history/deploylist/workStep`
    if(instrumentId) url += `?instrumentId=${instrumentId}`;
    if(instrumentId && startTime) url += `&startTime=${startTime}`
    else if(!instrumentId && startTime) url += `?startTime=${startTime}`;
    if(instrumentId && endTime) url += `&endTime=${endTime}`
    else if(!instrumentId && endTime) url += `?endTime=${endTime}`;
    return this.http.get(url).map(result => {
      return result.json();
    })
  }

  // 根据时间获取有公布数据的设备列表
  getInstrListByDate(date: string): Observable<any> {
    let url = `/a/history/deploylist/workStepInstrumentListByDate`
    if(date) url+=`?date=${date}`
    return this.http.get(url).map(result => {
      return result.json();
    })
  }  

  // 工步历史根据月份返回每天的测量数 
  getWorkStepInstrumentInfoListByDate(month: string): Observable<any> {
    let url = `http://47.92.167.96:8080/hnty/a/history/deploylist/getWorkStepInstrumentInfoListByDate`
    if(month) url+=`?date=${month}`
    return this.http.get(url).map(result => {
      return result.json();
    })
  }
  // 工步历史根据月份及仪器返回每天的公布数
  getWorkStepInfoListByInstruDate(month: string, id: string): Observable<any> {
    let url = `/a/history/deploylist/getWorkStepCountByDateInstru`
    if(month) url+=`?date=${month}`
    if(id) url+=`&instrumentId=${id}`
    return this.http.get(url).map(result => {
      return result.json();
    })
  }

}
