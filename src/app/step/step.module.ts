import { NgModule } from '@angular/core';
import { SharedModule } from '../shared/shared.module'
import { StepRouterModule } from './step-router.module'

import { AuthService } from '../core/auth.service'
import { StepService } from './services/step.service'

import { StepComponent } from './step/step.component';
import { StepListComponent } from './step-list/step-list.component';
import { StepStepsComponent } from './common/step-steps/step-steps.component';
import { SetpDialogComponent } from './common/setp-dialog/setp-dialog.component';
import { CalendarPanelComponent } from './common/calendar-panel/calendar-panel.component';
import { CalendarPanelContentComponent } from './common/calendar-panel-content/calendar-panel-content.component';

@NgModule({
  imports: [
    SharedModule,
    StepRouterModule
  ],
  declarations: [StepComponent, StepListComponent, StepStepsComponent, SetpDialogComponent, CalendarPanelComponent, CalendarPanelContentComponent],
  providers: [StepService, AuthService]
})
export class StepModule { }
