String.prototype.StringFormat = function () {
    if (arguments.length == 0) {
        return this;
    }
    for (var StringFormat_s = this, StringFormat_i = 0; StringFormat_i < arguments.length; StringFormat_i++) {
        StringFormat_s = StringFormat_s.replace(new RegExp("\\{" + StringFormat_i + "\\}", "g"), arguments[StringFormat_i]);
    }
    return StringFormat_s;
};

//#region 百度地图相关变量
var map=null;
var ORIGINAL_LNG=105.403119;    //地图原始中心点
var ORIGINAL_LAT=35.588449;
var ORIGINAL_ZOOM=9;            //地图原始缩放比例
var centerPoint=null;           //地图HOME中心点
var homeZoom=9;                 //地图HOME缩放比例
var LEVEL_MIN = 5;              //缩放界限
var LEVEL_MAX = 15;
var mapStyleIndex=0;            //当前地图样式索引
var currentMapStyle = ["midnight", "normal"];   //地图样式
var cityListControl = null;     //城市列表控件
var cityListVisible = false;
var myDrag=null;                //拉框放大控件
var dragZoom=false;
var mapStyle_home=[
    {
        "featureType": "water",
        "elementType": "all",
        "stylers": {
            "color": "#08304b"
        }
    },
    {
        "featureType": "highway",
        "elementType": "geometry.fill",
        "stylers": {
            "color": "#555",
            // "visibility": "off"
        }
    },
    {
        "featureType": "highway",
        "elementType": "geometry.stroke",
        "stylers": {
            "color": "#147a92",
            // "visibility": "off"
        }
    },
    {
        "featureType": "arterial",
        "elementType": "geometry.fill",
        "stylers": {
            "color": "#666"
        }
    },
    {
        "featureType": "arterial",
        "elementType": "geometry.stroke",
        "stylers": {
            "color": "#0b3d51"
        }
    },
    {
        "featureType": "local",
        "elementType": "geometry",
        "stylers": {
            "color": "#666"
        }
    },
    {
        "featureType": "land",
        "elementType": "all",
        "stylers": {
            "color": "#08304b",
        }
    },
    {
        "featureType": "railway",
        "elementType": "geometry.fill",
        "stylers": {
            "color": "#666",
            // "visibility": "off"
        }
    },
    {
        "featureType": "railway",
        "elementType": "geometry.stroke",
        "stylers": {
            "color": "#08304b",
            // "visibility": "off"
        }
    },
    {
        "featureType": "subway",
        "elementType": "geometry",
        "stylers": {
            "lightness": -70,
            //"visibility": "off"
        }
    },
    {
        "featureType": "building",
        "elementType": "geometry.fill",
        "stylers": {
            "color": "#000000"
        }
    },
    {
        "featureType": "all",
        "elementType": "labels.text.fill",
        "stylers": {
            "color": "#857f7f",
            //"visibility": "off"
        }
    },
    {
        "featureType": "country",
        "elementType": "labels",
        "stylers": {
            "color": "#DDDDDD",
            //"visibility": "off"
        }
    },
    {
        "featureType": "all",
        "elementType": "labels.text.stroke",
        "stylers": {
            "color": "#000000",
            "visibility": "off"
        }
    },
    {
        "featureType": "building",
        "elementType": "geometry",
        "stylers": {
            "color": "#022338"
        }
    },
    {
        "featureType": "green",
        "elementType": "geometry",
        "stylers": {
            "color": "#062032"
        }
    },
    {
        "featureType": "boundary",
        "elementType": "all",
        "stylers": {
            "color": "#0b539466",
        }
    },
    {
        "featureType": "manmade",
        "elementType": "all",
        "stylers": {
            "color": "#022338"
        }
    },
    {
        "featureType": "poi",
        "elementType": "all",
        "stylers": {
            "color": "#ffffff",
            // "visibility": "off"
        }
    },
    {                                       //边界颜色
        "featureType": "boundary",
        "elementType": "geometry.stroke",
        "stylers": {
            "color": "#00ffffff"
        }
    }
    ];        //Home的style,将来做成与地图级别对应的数组
var devMarks = [];              //地图Marks列表，用于点聚合
var devObjList = [];            //仪器对象列表

var areaName = "湖北省";         //初始定位
//endregion

//#region Echarts相关变量
var myChart=null;           //Echarts图
var myChart1=null;           //Echarts图
//#endregion

// 设置默认设备
var selectDev;

$(document).ready(function(){
    initRequest();

    // layer折叠panel 缩略图菜单
    layui.use('element', function() {
        var element = layui.element;
    })

    //监听缩略图列表收缩展开
    $(".toggle-button").click(function() {
        $('#panel-tips-wrap').toggleClass("active");
    })
    $(".header-toggle-button").click(function () {
        $('#header').toggleClass("active");
    })
    $("#totalRealtimePanel").hide();
    $("#geogPanel").hide();
    setTimeout(() => {
        factor = "CO2";
        if(factor != null) showMeasure();
    }, 1000);
});

//#region 从服务器请求数据
var host = window.location.host;
var socket;
function initRequest() {
    $.ajax({
        url: '/'+location.pathname.split("/")[1]+'/a/situation/currentsituation/datas',      //环境态势测试数据获取
        type: 'GET',
        dataType: "json",
        data: {},
        success: function (data) {    
            ownerDevList = data.datas;
            connect();    //准备接收实时数据

            //此处用./js/simulationData.js中的模拟数据代替
            initialMap();

            // 初始化搜索参数
            initSerach("湖北省", "3012H", "在线");
            
            //初始化实时上传面板
            selectDev = ownerDevList.filter(item => item.deviceId == 'LH12A08885739X')[0]?ownerDevList.filter(item => item.deviceId == 'LH12A08885739X')[0]:ownerDevList[0];
            initSingleRealtimePanel(selectDev.deviceId);
            
            //初始化综合实时面板
            initPanel('totalRealtimePanel', data.datas);
            
            //初始化参数超标面板
        }
    })
    
    $.ajax({
        url: '/'+location.pathname.split("/")[1]+'/a/info/alarm/datas',
        type: 'get',
        dataType: 'json',
        data: {},
        success: function (data) {
            initPanel('geogPanel', data.datas)
        }
    })
}

function setConnected(connected) {
    document.getElementById('connect').disabled = connected;
    document.getElementById('disconnect').disabled = !connected;
    document.getElementById('response').innerHTML = '';
}

function connect() {
    if ('WebSocket' in window) {
        //浏览器支持WebSocket
        console.log('Websocket supported');
        socket = new WebSocket('ws://' + window.location.host + '/hnty' + '/real');

        socket.onopen = function() {
            // setConnected(true);
        }

        socket.onclose = function() {
        }

        //WebSocket收到服务器的消息时，触发onmessage方法
        socket.onmessage = function(evt) {
            var received_msg = JSON.parse(evt.data);
            showMessage(received_msg);
        }
    } else {
        //浏览器不支持WebSocket时的处理
        alert("本浏览器不支持WebSocket");
    }
}

function disconnect() {
    setConnected(false);
}

//发送消息至服务器
function sendMsg2Server() {
    var message = document.getElementById('message').value;
    socket.send(JSON.stringify({
        'message' : message
    }));
}

//页面显示从服务器收到的数据
function showMessage(message) {
    // 测试websocket更新数据不校验deviceId
    if(message.Equ_ID && message.Equ_ID == selectDev.deviceId) {
        var jd = $($(`#measure-body div`).eq(2));
        $($(`#measure-body div`).eq(1)).text(message.pa1)
        $($(`#measure-body div`).eq(3)).text(message.pa2)
        $($(`#measure-body div`).eq(5)).text(message.pa3)
        $($(`#measure-body div`).eq(7)).text(message.pa4)
        $($(`#measure-body div`).eq(9)).text(message.pa5)
        $($(`#measure-body div`).eq(11)).text(message.pa6)
        $($(`#measure-body div`).eq(13)).text(message.pa7)
        $($(`#measure-body div`).eq(15)).text(message.pa8)

        myChart1.setOption({
            series: [{
                data: [message.pa1, message.pa2, message.pa3, message.pa4, message.pa5, message.pa6, message.pa7, message.pa8]
            }]
        }) 
    }
}
//#endregion

//#region 初始化地图，添加仪器标记点
function initialMap() {
    centerPoint = new BMap.Point(ORIGINAL_LNG, ORIGINAL_LAT);       // 地图中心点
    myMap = new BMap.Map("mapShow");      							// 创建地图实例
    mapGoOriginal();                                                // 设置中心点坐标和地图级别

    myMap.addControl(new BMap.NavigationControl({offset: new BMap.Size(50, 150) }));                   // 平移缩放控件
    myMap.addControl(new BMap.ScaleControl());                        // 比例尺控件
    myMap.addControl(new BMap.OverviewMapControl());                  // 缩略图控件，右下角小箭头
    myMap.addControl(new BMap.MapTypeControl({anchor: BMAP_ANCHOR_BOTTOM_RIGHT, mapTypes: [BMAP_NORMAL_MAP,BMAP_SATELLITE_MAP]}));// 地图、卫星切换
    myMap.enableScrollWheelZoom();                                    // 启用滚轮放大缩小
    myMap.enableContinuousZoom();                                     // 无级缩放
    myMap.setMinZoom(LEVEL_MIN);                                      // 限制缩放级别
    myMap.setMaxZoom(LEVEL_MAX);

    //地图缩放监听事件 属性“moveend”可监听移动事件
    myMap.addEventListener("zoomend",
        function () {           
            if ((myMap.getZoom() >= LEVEL_MIN) && (myMap.getZoom() <= LEVEL_MAX)){
                if(myMap.getZoom() <= LEVEL_MIN) {  //根据不同缩放级别设置style
                    // alert('LEVEL_MIN');
                    mapGoOriginal();
                }
                else{
                    myMap.setMapStyle({style:currentMapStyle[mapStyleIndex]});
                }
            };
        });
    //点击地图关闭查询面板
    myMap.addEventListener("click",
        function(){
            devSearchPanelShow(false);
            dataSearchPanelShow(false);
            mapvHide();
        });
    /*myMap.addEventListener("rightclick",
        function(){
            alert("rightclick");
            myDrag.Close();
        });*/
    //监听地图缩放事件
    //myMap.onChange();
    //myMap.onResize();

    // 显示湖北省及省界
    gotoArea(areaName);

    createDevMarks(ownerDevList);   // 初始化图标相关信息
    appendDevTable(ownerDevList);   // 填充仪器静态信息列表
    appendMeasureTable(measureDataList);
}

// 创建仪器标记点
function createDevMarks(devList){
    for (var i = 0; i < devList.length; i++) {
        var aDev= new DeviceModel(myMap, devList[i], devMarks);
        devObjList.push(aDev);
    }

    var markerClusterer = new BMapLib.MarkerClusterer(myMap, {markers: devMarks});
}

// 地图回归原点
function mapGoOriginal() {
    myMap.centerAndZoom(new BMap.Point(ORIGINAL_LNG, ORIGINAL_LAT),ORIGINAL_ZOOM);   //回归初始状态
    myMap.setMapStyle({styleJson:mapStyle_home});
    // myMap.setMapStyle({style: currentMapStyle[mapStyleIndex]});
}

// 拖动指定id的DIV
function dragPanel(objId){
    var div =  document.getElementById(objId);
    var dragFlag = false;
    var x, y;

    div.onmousedown = function (e) {
        e = e || window.event;
        x = e.clientX - div.offsetLeft;
        y = e.clientY - div.offsetTop;
        dragFlag = true;

        div.style.cursor = "move";
    };

    document.onmousemove = function (e) {
        if (dragFlag) {
            e = e || window.event;
            div.style.left = e.clientX - x + "px";
            div.style.top = e.clientY - y + "px";
        }
    };

    document.onmouseup = function (e) {
        dragFlag = false;
        div.style.cursor = "default";
    };
}

function gotoArea(areaName){
    var bdary = new BMap.Boundary();
    bdary.get(areaName, function(rs){       //获取行政区域
        myMap.clearOverlays();        //清除地图覆盖物
        var count = rs.boundaries.length; //行政区域的点有多少个
        if (count === 0) {
            alert('未能获取当前输入行政区域');
            return ;
        }
        var pointArray = [];
        for (var i = 0; i < count; i++) {
            var ply = new BMap.Polygon(rs.boundaries[i],
                {strokeWeight: 4, strokeColor: "#0466fd", strokeOpacity:0.8, fillColor:"#dde5fa", fillOpacity: 0.1}); //建立多边形覆盖物
            myMap.addOverlay(ply);  //添加覆盖物
            pointArray = pointArray.concat(ply.getPath());
        }
        myMap.setViewport(pointArray);    //调整视野
        myMap.setZoom(ORIGINAL_ZOOM);

        DropdownSetHome();
    });
}
//#endregion

//#region 组合查询
// 提交参数查询
function searchGo(keyCondition){
    /*var local = new BMap.LocalSearch(myMap, {
        renderOptions:{map: myMap, panel:"r-result"},
        pageCapacity:5
    });
    local.searchInBounds(keyCondition, myMap.getBounds());*/
}

// 显示/隐藏组合查询主面板 最大化 最小化
function complexSearchPanelShow(bVisible) {
    var displayStyle = "none";
    if (bVisible) {
        displayStyle = "block";
        complexSearchMinShow(false);
    }
    document.getElementById("searchdiv").style.display = displayStyle;
}
function complexSearchMinShow(bVisible) {
    var displayStyle = "none";
    if (bVisible) {
        displayStyle = "block";
        complexSearchPanelShow(false);
    }
    document.getElementById("searchdivMini").style.display = displayStyle;
}

// 查询仪器查询面板的显示/隐藏
function devSearchPanelShow(bVisible) {
    var displayStyle = "none";
    if (bVisible) {
        displayStyle = "block";
        dataSearchPanelShow(false);
    }
    document.getElementById("devSearchPanel").style.display = displayStyle;
}

// 查询测量结果查询面板的显示/隐藏
function dataSearchPanelShow(bVisible) {
    var displayStyle = "none";
    if (bVisible) {
        displayStyle = "block";
        devSearchPanelShow(false);
    }
    document.getElementById("dataSearchPanel").style.display = displayStyle;
}

// 按地域查询下拉列
$(function () {
    new DropDownClick($('#dropdownArea'));
});
function areaSelected(area){
    document.getElementById('spanArea').innerHTML = area;
    //发送按地域查询指令
}

// 按类型查询下拉列
$(function () {
    new DropDownClick($('#dropdownType'));
});
function typeSelected(type){
    document.getElementById('spanType').innerHTML = type;
    //发送按类型查询指令
}

// 按状态查询下拉列
$(function () {
    new DropDownClick($('#dropdownStatus'));
});
function statusSelected(status){
    document.getElementById('spanStatus').innerHTML = status;
    //发送按状态查询指令
}

//仪器列表 按编号排序
var devCodeUp = true;
function devCodeSort(objID) {
    devCodeUp = !devCodeUp;
    if(devCodeUp){     //升序
        document.getElementById(objID).innerHTML = '仪器编号 ▲';
    }
    else{           //降序
        document.getElementById(objID).innerHTML = '仪器编号 ▼';
    }
    //排序
}

//测量列表 按仪器编号排序
var measureCodeUp = true;
function measureDevSort(objID) {
    measureCodeUp = !measureCodeUp;
    if(measureCodeUp){     //升序
        document.getElementById(objID).innerHTML = '仪器编号 ▲';
    }
    else{           //降序
        document.getElementById(objID).innerHTML = '仪器编号 ▼';
    }
    //排序
}
//测量列表 按测量批号排序
var batchNumUp = true;
function batchNumberSort(objID) {
    batchNumUp = !batchNumUp;
    if(batchNumUp){     //升序
        document.getElementById(objID).innerHTML = '测量批号 ▲';
    }
    else{           //降序
        document.getElementById(objID).innerHTML = '测量批号 ▼';
    }
}
//测量列表 按时间排序
var sortTimeUp = true;
function measureTimeSort(objID){
    sortTimeUp = !sortTimeUp;
    if(sortTimeUp){     //升序
        document.getElementById(objID).innerHTML = '测量时间 ▲';
    }
    else{           //降序
        document.getElementById(objID).innerHTML = '测量时间 ▼';
    }
    //排序
}
//#endregion

//#region 显示仪器为主的图表
function showDevInfowin() {
    document.getElementById("devSearchPanel").style.display = "none";
    // document.getElementById("dataGraph").style.display = "block";
    // document.getElementById("dataTable").style.display = "block";
}

function appendDevTable(ownerDevList){
    var tableNode=document.getElementById("devListTable");//获得Table对象

    for(var i = 0; i < ownerDevList.length; i++){
        var trNode=tableNode.insertRow();//加一行
        trNode.className = "datagrid-cell";

        var tdNode = trNode.insertCell();
        tdNode.className = "datagrid-cell cell-c1";//CSS样式
        var spanNode = document.createElement('span');
        spanNode.className ="cellDevID";
        spanNode.innerHTML = ownerDevList[i].deviceId;
        tdNode.appendChild(spanNode);
        (function(deviceId){
            $(tdNode).on('click', function() {
                onSelectDevice(deviceId)
            })
        })(ownerDevList[i].deviceId);
        var tdNode = trNode.insertCell();
        tdNode.className = "datagrid-cell cell-c2";
        tdNode.innerHTML = ownerDevList[i].deviceAlias;
        var tdNode = trNode.insertCell();
        tdNode.className = "datagrid-cell cell-c3";
        tdNode.innerHTML = ownerDevList[i].devicePurUnitName;
        var tdNode = trNode.insertCell();
        tdNode.className = "datagrid-cell cell-c4";
        tdNode.innerHTML = ownerDevList[i].areaName;
    }
}

function showGraphwin() {
    document.getElementById("dataGraph").style.display = "none";
    instrumentId = null;
}

function showTablewin() {
    document.getElementById("dataTable").style.display = "none";
    instrumentId = null;
}

//从列表中选择仪器id
var instrumentId = null;
$(function(){
    $(document).on('click', '.cellDevID', function(){
        // alert(this.innerHTML);
        instrumentId = this.innerHTML;
        if(instrumentId != null) {
            var aDev = searchById(instrumentId);
            aDev.getMarker().setAnimation(BMAP_ANIMATION_BOUNCE);
            if (aDev != null) {
                devIsSelected(aDev);

                drawBarChart();     //Echarts 图
                drawBarChart1();     //Echarts 图
            }
        }
    });
});

//Echarts
function drawBarChart(){
    require.config({
        paths: {
            echarts: 'http://echarts.baidu.com/build/dist'
        }
    });
    require(
        [
            'echarts',
            'echarts/chart/line',   // 按需加载所需图表，如需动态类型切换功能，别忘了同时加载相应图表  
            'echarts/chart/bar'
        ],
        function (ec) {
            myChart = ec.init(document.getElementById('echartsBox'),'macarons');
            var option = {
                title:{
                    text:instrumentId,
                    textStyle: {
                        fontSize: 12,
                        color: "#005eb2",
                        fontFamily: "Microsoft YaHei"
                    }
                },
                tooltip : {
                    trigger: 'axis',             //item:只显示该点的数据，axis:显示该列下所有坐标轴所对应的数据
                    textStyle: {
                        fontSize: 10,
                        color: "#fff",
                    }
                },
                legend: {
                    data:['最高','最低']
                },
                toolbox: {
                    show : true,
                    orient: 'vertical',         // 布局方式，默认为水平布局，可选为：'horizontal' ¦ 'vertical'
                    x: 'right',                 // 水平安放位置，默认为全图右对齐，可选为：'center' ¦ 'left' ¦ 'right' ¦ {number}（x坐标，单位px）
                    y: 'top',                // 垂直安放位置，默认为全图顶端，可选为：'top' ¦ 'bottom' ¦ 'center' ¦ {number}（y坐标，单位px）
                    color : ['#1e90ff','#22bb22','#4b0082','#d2691e'],
                    backgroundColor: 'rgba(0,0,0,0)', // 工具箱背景颜色
                    borderColor: '#ccc',        // 工具箱边框颜色
                    borderWidth: 0,             // 工具箱边框线宽，单位px，默认为0（无边框）
                    padding: 5,                 // 工具箱内边距，单位px，默认各方向内边距为5，
                    showTitle: true,
                    feature : {
                        mark : {
                            show : true,
                            title : {
                                mark : '辅助线-开关',
                                markUndo : '辅助线-删除',
                                markClear : '辅助线-清空'
                            },
                            lineStyle : {
                                width : 1,
                                color : '#1e90ff',
                                type : 'dashed'
                            }
                        },
                        magicType: {
                            show : true,
                            title : {
                                line : '动态类型切换-折线图',
                                bar : '动态类型切换-柱形图',
                                stack : '动态类型切换-堆积',
                                tiled : '动态类型切换-平铺'
                            },
                            type : ['line', 'bar', 'stack', 'tiled']
                        },
                        restore : {
                            show : true,
                            title : '还原',
                            color : 'black'
                        },
                        saveAsImage : {
                            show : true,
                            title : '保存为图片',
                            type : 'jpeg',
                            lang : ['点击本地保存']
                        },
                    }
                },
                xAxis : [
                    {
                        type : 'category',
                        boundaryGap : false,
                        axisLabel: {         //调整x轴的lable  
                            textStyle: {
                                fontSize: 10,
                            }
                        },
                        splitLine: {
                            show: false
                        },
                        axisLabel: { show: true, textStyle: { color: '#ddd' } } ,
                        data : function (){
                            var list = [];
                            for (var i = 1; i <= 30; i++) {
                                list.push('2018-04-' + i);
                            }
                            return list;
                        }()
                    }
                ],
                yAxis : [
                    {
                        type : 'value',
                        axisLabel: {         //调整y轴的lable  
                            textStyle: {
                                fontSize: 10
                            },
                            //show:false
                        },
                        splitLine: {
                            show: false
                        },
                        axisLabel: { show: true, textStyle: { color: '#ddd' } } ,
                        // axisTick:{
                        //     show:false
                        // }
                    }
                ],
                series : [
                    {
                        name:'最高',
                        type:'line',
                        data:function (){
                            var list = [];
                            for (var i = 1; i <= 30; i++) {
                                list.push(Math.round(Math.random()* 30));
                            }
                            return list;
                        }()
                    },
                    {
                        name:'最低',
                        type:'line',
                        data:function (){
                            var list = [];
                            for (var i = 1; i <= 30; i++) {
                                list.push(Math.round(Math.random()* 10));
                            }
                            return list;
                        }()
                    }
                ],
                grid: {
                    x: 30,
                    y: 40,
                    x2: 40,
                    y2: 40,
                    /*borderWidth: 4,
                    borderColor: '#ff0000'*/
                    // containLabel: true
                }
            };

            myChart.setOption(option);
        }
    );

    // 页面加载完毕 相当于 document.ready，{代码}
    $(function(){
    })
}

function drawBarChart1(){
    require.config({
        paths: {
            echarts: 'http://echarts.baidu.com/build/dist'
        }
    });
    require(
        [
            'echarts',
            'echarts/chart/line',   // 按需加载所需图表，如需动态类型切换功能，别忘了同时加载相应图表  
            'echarts/chart/bar'
        ],
        function (ec) {
            myChart1 = ec.init(document.getElementById('echartsBox1'),'macarons');
            var option = {
                title : {
                    text: instrumentId,
                    x:'center'
                },
                tooltip : {
                    trigger: 'axis',
                    axisPointer : {            // 坐标轴指示器，坐标轴触发有效
                        type : 'shadow'        // 默认为直线，可选为：'line' | 'shadow'
                    }
                },
                grid: {//直角坐标系控制
                    show: false,
                    x: 25,//grid 组件离容器左侧的距离
                    y: 26,
                    x2: 25,
                    y2: 26,
                    borderWidth: 2,
                    borderColor: '#53c6f8',
                    backgroundColor: 'rgba(255,255, 255, 0.05)'
                },
                xAxis : [
                    {
                        type : 'category',
                        data : ['O2','SO2','NO','NO2','NOx','CO','co2','H2S'],
                        axisTick: {
                            alignWithLabel: true
                        },
                        splitLine: {
                            show: false
                        },
                        axisLabel: { show: true, textStyle: { color: '#fff' } } 
                    }
                ],
                yAxis : [
                    {
                        type : 'value',
                        splitLine: {
                            show: false
                        },
                        axisLabel: { show: true, textStyle: { color: '#fff' } } 
                    }
                ],
                series : [
                    {
                        type:'bar',
                        barWidth:12,
                        data:[21,10,52,200,334,390,330,220],
                        itemStyle: {
                            normal:{
                                color: function (params){
                                    var colorList = ['#00e5ff','#3f51b5','#1e88e5','#26c6da','#304ffe','#1de9b6','#83bff6','#40c4ff'];
                                    return colorList[params.dataIndex];
                                }
                            },
                        },
                    }
                ],
                grid: {
                    x: 30,
                    y: 40,
                    x2: 40,
                    y2: 40,
                }
            };

            myChart1.setOption(option);
        }
    );

    // 页面加载完毕 相当于 document.ready，{代码}
    $(function(){
    })
}
//Echarts 变换主题
$("[name='echartTheme']").on("change",
    function (e) {
        myChart.setTheme($(e.target).val());
        //alert($(e.target).val())
    }
);

//#endregion

//#region 显示测量值为主的热力图等
var countMeasure=0;
var mapvLayer1, mapvLayer2, mapvLayer3;
function showMeasure() {
    document.getElementById("mapvStyle").style.display = "block";

    countMeasure = 0;
    showMapvStatus();
}

function appendMeasureTable(measureDataList){
    var tableNode=document.getElementById("measureListTable");//获得Table对象

    for(var i = 0; i < measureDataList.length; i++){
        var trNode=tableNode.insertRow();//加一行
        trNode.className = "datagrid-cell";

        var tdNode = trNode.insertCell();
        tdNode.className = "datagrid-cell cell-c1";
        var spanNode = document.createElement('span');
        spanNode.className ="cellDevID";
        spanNode.innerHTML = measureDataList[i].deviceId;
        tdNode.appendChild(spanNode);
        var tdNode = trNode.insertCell();
        tdNode.className = "datagrid-cell cell-c22";
        tdNode.innerHTML = measureDataList[i].batchNumber;
        var tdNode = trNode.insertCell();
        tdNode.className = "datagrid-cell cell-c3";
        tdNode.innerHTML = measureDataList[i].measureDate;
    }
}

//选择测量参数按钮
var factor=null;
$(function(){
    $('.paraTable td').click(function(){
        //alert(this.innerHTML);
        factor = this.innerHTML;
        if(factor != null) showMeasure();
    });
});

$("[name='mapvSelect']").on("change",
    function (e) {
        switch ($(e.target).val()) {
            case "1":
                countMeasure = 0;
                break;
            case "2":
                countMeasure = 1;
                break;
            case "3":
                countMeasure = 2;
                break;
            case "4":
                countMeasure = 3;
                break;
        }
        // alert(countMeasure);
        showMapvStatus();
    }
);

//热力图数据
var dataSet=null;
//每收到实时数据，调用一次
function updateValue(){
    var data = [];
    var factorValue = 0;

    for(var i = 0; i < measureDataList.length; i++){
        switch (factor){
            case "SO2":
                factorValue = measureDataList[i].resultSO2Avg;
                break;
            case "NO":
                factorValue = measureDataList[i].resultNOAvg;
                break;
            case "NO2":
                factorValue = measureDataList[i].resultNO2Avg;
                break;
            case "NOx":
                factorValue = measureDataList[i].resultNOXAvg;
                break;
            case "CO":
                factorValue = measureDataList[i].resultCOAvg;
                break;
            case "H2S":
                factorValue = measureDataList[i].resultH2SAvg;
                break;
            case "CO2":
                factorValue = measureDataList[i].resultCO2Avg;
                break;
            case "O2":
                factorValue = measureDataList[i].resultO2Avg;
                break;
        }
        data.push({
            geometry: {         //地理位置，经纬度以城市中心加随机偏移量生成。实际数据用仪器经纬度
                type: 'Point',
                coordinates: [ownerDevList[i].longitude, ownerDevList[i].latitude]
            },
            count: factorValue,      //绘图值 实际数据用仪器测量数据
            //time: 100 * Math.random()                  //为了达到闪烁效果的时间间隔，实际数据应该不用闪烁，按真实测量值显示
        });
    }

    if(dataSet == null) {   // 构造数据,能收到实时数据时，以实时数据显示
        dataSet = new mapv.DataSet(data);
    }
    else{
        dataSet.set(data); // 修改数据
    }
}

function showMapvStatus() {
    updateValue();

    if(countMeasure == 0) {         //热力图
        countMeasure++;

        var options = {
            size: 60,       //圆直径
            gradient: {0.25: "rgb(0,0,255)", 0.55: "rgb(0,255,0)", 0.85: "yellow", 1.0: "rgb(255,0,0)"},//颜色梯度
            max: 300,       //最大值
            draw: 'heatmap' //图形类别
        }
        if(mapvLayer2 != null) mapvLayer2.hide();
        if(mapvLayer3 != null) mapvLayer3.hide();
        if(mapvLayer1 != null){
            mapvLayer1.show();
        }
        else {
            mapvLayer1 = new mapv.baiduMapLayer(myMap, dataSet, options);
        }
    }
    else {
        if (countMeasure == 1) {         //简单点加亮色气泡
            countMeasure++;
            var options = {
                fillStyle: 'rgba(55, 50, 250, 0.5)',
                globalCompositeOperation: "lighter",
                size: 60,
                max: 300,
                draw: 'simple'
            }
            if(mapvLayer1 != null) mapvLayer1.hide();
            if(mapvLayer3 != null) mapvLayer3.hide();
            if(mapvLayer2 != null){
                mapvLayer2.show();
            }
            else {
                mapvLayer2 = new mapv.baiduMapLayer(myMap, dataSet, options);
            }
        }
        else {
            if (countMeasure == 2) {//方块标数字
                countMeasure == 0;
                var options = {
                    fillStyle: 'rgba(55, 50, 250, 0.8)',
                    shadowColor: 'rgba(255, 250, 50, 1)',
                    shadowBlur: 20,
                    size: 60,
                    globalAlpha: 0.5,
                    max: 300,
                    label: {
                        show: true,
                        fillStyle: 'white',
                        shadowColor: 'yellow',
                        font: '16px Arial',
                        shadowBlur: 10,
                    },
                    gradient: {0.25: "rgb(0,0,255)", 0.55: "rgb(0,255,0)", 0.85: "yellow", 1.0: "rgb(255,0,0)"},
                    draw: 'grid'
                }
                if(mapvLayer1 != null) mapvLayer1.hide();
                if(mapvLayer2 != null) mapvLayer2.hide();
                if(mapvLayer3 != null){
                    mapvLayer3.show();
                }
                else {
                    mapvLayer3 = new mapv.baiduMapLayer(myMap, dataSet, options);

                }
            }
        }
    }
}

function mapvHide() {
    if(mapvLayer1 != null) mapvLayer1.hide();
    if(mapvLayer2 != null) mapvLayer2.hide();
    if(mapvLayer3 != null) mapvLayer3.hide();
    document.getElementById("mapvStyle").style.display = "none";
}
//#endregion

//#region "工具"下拉列表处理
function DropDownClick(e1) {
    this.DropDownList = e1;
    this.initEvents();
}
DropDownClick.prototype = {
    initEvents: function () {
        var obj = this;
        obj.DropDownList.on('click', function (event) {
            $(this).toggleClass('active');
            event.stopPropagation();
        });
    }
}
$(function () {
    new DropDownClick($('#dropdownTool'));
});

//测距
function DropdownDistance(){
    var myDis = new BMapLib.DistanceTool(myMap);
    myDis.open();  //开启鼠标测距

    //myDis.close();  //关闭鼠标测距
}
//城市列表
function DropdownCity(){
    cityListVisible = !cityListVisible;

    if(cityListControl == null) {
        cityListControl = new BMap.CityListControl({
            anchor: BMAP_ANCHOR_TOP_LEFT,
            offset: new BMap.Size(340, 60),  //位置偏移，左，上

            /*            onChangeBefore: function(){      // 切换城市之前事件
                           alert('before');
                        },

                        onChangeAfter:function(){        // 切换城市之后事件
                          alert('after');
                        }*/
        });
    }

    if(cityListVisible) {
        myMap.addControl(cityListControl);
    }
    else{
        myMap.removeControl(cityListControl);
    }
}
//归心
function DropdownHome(){
    if(homeZoom <= LEVEL_MIN) {  //根据不同缩放级别设置style
        mapGoOriginal();
    }
    else {
        myMap.centerAndZoom(centerPoint, homeZoom);
        myMap.setMapStyle({style: currentMapStyle[mapStyleIndex]});
    }
}
//框选
function DropdownSelect() {
    dragZoom = !dragZoom;
    if(dragZoom) {
        myDrag = new BMapLib.RectangleZoom(myMap, {
            followText: "拖拽鼠标进行操作"
        });
        myDrag.open();  //开启拉框放大
    }
    else {
        myDrag.close();
    }
}

//#endregion

//#region "筛选"下拉列表处理
$(function () {
    new DropDownClick($('#dropdownFilter'));
});
function DropDownBaseInfo(){
    alert("点击了基础信息！");
}
function DropdownMeasurem(){
    alert("点击了测量信息！");
}
function DropdownAlarm(){
    alert("点击了事件报警！");
}
function DropdownStatistics() {
    alert("点击了综合统计！");
}
//#endregion

//#region "设置"下拉列表处理
$(function () {
    new DropDownClick($('#dropdownDevSet'));
});
//深浅色模式切换
function DropDownMapStyle(){
    if(myMap.getZoom() != LEVEL_MIN) {
        if (mapStyleIndex == 1) {
            mapStyleIndex = 0;
        }
        else {
            mapStyleIndex = 1;
        }
        myMap.setMapStyle({style: currentMapStyle[mapStyleIndex]});
    }
}
function DropdownAlarmWin(){
    alert("点击了报警弹窗！");
}
function DropdownShowToolbar(){
    alert("点击了工具条开！");
}

//获取当前地图中心点和缩放比例作为新的归心点
function DropdownSetHome(){
    centerPoint = myMap.getCenter();
    homeZoom = myMap.getZoom();
}
//#endregion

//#region 仪器列表的操作
//按仪器id查找，返回devObj，找不到返回null
function searchById(devId){
    var aDev = null;
    for(var i=0; i<devObjList.length; i++){
        if(devObjList[i].getDevId() == devId){
            aDev = devObjList[i];
            break;
        }
    }
    return aDev;
}

function devIsSelected(aDev){
    //图标弹跳
    setTimeout(function(){
        aDev.getMarker().setAnimation(null);    //二秒后停止跳动
    }, 2000);

    //地图中心移动，缩放倍数调整
}
//#endregion


/**
 * initSerach初始化搜索参数
 */
function initSerach(area, type, status) {
    $("#spanArea").text(area);
    $("#spanType").text(type)
    $("#spanStatus").text(status)
}

/**
 * 折叠图表panel
 */
function togglePanel(id) {
    $('#'+id).toggle();
}


/**
 * 初始化singleRealtimePanel
 */

function initSingleRealtimePanel(deviceId) {
    instrumentId = deviceId;
    var aDev = searchById(deviceId)
    if(deviceId != null) {
        aDev.getMarker().setAnimation(BMAP_ANIMATION_BOUNCE);
        if (aDev != null) {
            devIsSelected(aDev);

            drawBarChart();     //Echarts 图
            // drawBarChart1();     //Echarts 图
        }
    }

    // 初始化设备信息
    $($(`#singleRealtimePanel li div`).eq(1)).text(aDev.getBaseInfo().areaName)
    $($(`#singleRealtimePanel li div`).eq(3)).text(aDev.getBaseInfo().longitude)
    $($(`#singleRealtimePanel li div`).eq(5)).text(aDev.getBaseInfo().latitude)
    $($(`#singleRealtimePanel li div`).eq(7)).text(aDev.getBaseInfo().deviceId)
    $($(`#singleRealtimePanel li div`).eq(9)).text(aDev.getBaseInfo().deviceType)
    $($(`#singleRealtimePanel li div`).eq(11)).text(aDev.getBaseInfo().deviceName)
    $($(`#singleRealtimePanel li div`).eq(13)).text(aDev.getBaseInfo().deviceProUnitName)
    $($(`#singleRealtimePanel li div`).eq(15)).text(aDev.getBaseInfo().devicePurUnitName)
    $($(`#singleRealtimePanel li div`).eq(17)).text(aDev.getBaseInfo().deployDate)
}


/**
 * 初始化panel
 */
// <!--页面滚动动画方法-->
function initPanel(id, data) {
    var totalRealtimePanel = document.getElementById(id);
    var ulWrap = $("#" + id + " tbody")[0];
    for (var i = 0; i < data.length; i++) {
        var lis = document.createElement('tr');
        if (id == 'totalRealtimePanel') {
            lis.innerHTML += '<td>' + data[i].areaName + '</td>' +
                '<td>' + data[i].deviceId + '</td>' +
                '<td>' + data[i].deviceType + '</td>' +
                '<td>' + data[i].onlineState + '</td>' +
                '<td>' + data[i].powerOnTime + '</td>';
//                '<td>' + data[i].usingDate + '</td>';
            ulWrap.appendChild(lis);
        } 
        else if(id = "geogPanel") {
            lis.innerHTML += '<td>' + data[i].instrumentID + '</td>' +
                '<td>' + data[i].dataItem + '</td>' +
                //'<td>' + data[i].threshold2 + '</td>' +
                //'<td>' + data[i].threshold1 + '</td>' +
                '<td>' + data[i].currentValue + '</td>' +
                '<td>' + data[i].alarmTime + '</td>';
            ulWrap.appendChild(lis);
        }
    }
    setInterval(function () {
        $("#" + id + " tbody").animate({
            marginTop: '-30px'
        }, function () {
            $(this).css({ marginTop: "0px" }).find("tr:first").appendTo(this);
        })
    }, 2000);
}

function togglePanel(id) {
    $('#'+id).toggle();
}

function onSelectDevice(deviceId) {
    if(deviceId != null) {
        var deviceMarker = searchById(deviceId);
        if(deviceMarker == null)  return ;
        myMap.panTo(deviceMarker.devMarker.getPosition());
        deviceMarker.devMarker.setAnimation(BMAP_ANIMATION_BOUNCE);
        selectDev = deviceMarker.baseInfoJson;
        initSingleRealtimePanel(selectDev.deviceId);
        var jd = $($(`#measure-body div`).eq(2));
        $($(`#measure-body div`).eq(1)).text("NaN")
        $($(`#measure-body div`).eq(3)).text("NaN")
        $($(`#measure-body div`).eq(5)).text("NaN")
        $($(`#measure-body div`).eq(7)).text("NaN")
        $($(`#measure-body div`).eq(9)).text("NaN")
        $($(`#measure-body div`).eq(11)).text("NaN")
        $($(`#measure-body div`).eq(13)).text("NaN")
        $($(`#measure-body div`).eq(15)).text("NaN")
        return;
    }
}
