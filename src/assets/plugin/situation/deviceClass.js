//仪器模板 构造时传入基本信息
var enumOnlineStatus = ["下线", "上线", "工作", "报警"];

function DeviceModel(thisMap, baseInfo, devMarks) {
    this.baseInfoJson = baseInfo;       //JSON格式
    this.measureInfoJson = "";

    //public方法 动态原型方式定义
    if (typeof DeviceModel._initialized == "undefined") {
        DeviceModel._initialized = true;        //只创建一次

        //基本信息赋值/获取
        DeviceModel.prototype.setBaseInfo = function (baseInfo) {
            if (baseInfo.devId == this.baseInfoJson.devId) {      //devID 不可更改
                this.baseInfoJson = baseInfo;   //设置基本信息
                setMyBaseWin();                 //同步修改基本信息窗
            }
        };
        DeviceModel.prototype.getBaseInfo = function () {
            return (this.baseInfoJson);               //返回基础信息 JSON格式
        };
        DeviceModel.prototype.getDevId = function () {
            return (this.baseInfoJson.deviceId);         //返回仪器id
        };

        //测量信息赋值/获取
        DeviceModel.prototype.setMeasureData = function (measureData) {
            this.measureInfoJson = measureData;     //设置测量信息
            setMyMeasureWin();                      //同步修改测量信息窗
        };
        DeviceModel.prototype.getMeasureData = function () {
            return (this.measureInfoJson);               //返回测量信息 JSON格式
        };
        DeviceModel.prototype.getBatchNum = function () {
            return (this.measureInfoJson.batchNumber);   //返回测量id
        };

        //提供地图marker对象
        DeviceModel.prototype.getMarker = function () {
            return (this.devMarker);
        }
    }

    initInfoWin();              //创建实例的时候初始化信息窗口样式

    //属性赋值
    this.devMarker = createDevMark(thisMap, baseInfo, devMarks);
}

//private方法
//初始化信息窗口模板
function initInfoWin(){
    //定义仪器基础信息窗口样式
    devBaseWinStyle = {
        title: '<p style="margin:0;line-height:14px;font-size:14px"><b>仪器信息</b><br/>-----------------------------</p>',	//标题
        width: 240,									//宽度
        height: 180,								//高度
    }
    devBaseTemplate = '<div style="margin:0;line-height:22px;padding:2px;font-size:14px">' +
        '<p>仪器编号：{0}<br/>' +
        '仪器名称：{1}<br/>' +
        '生产厂家：{2}<br/>' +
        '所在位置：{3}<br/>' +
        '购买单位：{4}<br/>' +
        '在线状态：{5}<br/>' +
        '健康状态：{6}</p></div>';

    //定义测量数据窗口样式
    measureWinStyle = {
        title: '<p style="margin:0;line-height:14px;font-size:14px"><b>测量信息</b><br/>-----------------------------</p>',	//标题
        width: 140,									//宽度
        height: 180,								//高度
    };
    measureTemplate = '<div style="margin:0;line-height:22px;padding:2px;font-size:14px">' +
        '<p>仪器编号：{0}<br/>' +
        '测量时间：{1}<br/>' +
        'O2： {2}   CO： {3}<br/>' +
        'CO2：{4}   SO2：{5}<br/>' +
        'NO： {6}   NO2：{7}<br/>' +
        'NOx：{8}   H2S：{9}</p></div>';
}

/**
 * 引入layer
 */
var layer;
layui.use(['layer'], function() {
    layer = layui.layer;
})

//创建Maker
function createDevMark(thisMap, baseInfo, devMarks){
    var devIconWidth = 22;
    var devIconHeight = 32;
    var imgName = "";
    var markPoint = new BMap.Point(parseFloat(baseInfo.longitude), parseFloat(baseInfo.latitude));
    switch (baseInfo.onlineState) {                 //不同状态用不同图标 尺寸32*32
        case enumOnlineStatus[0]:
            imgName = "../../../../static/Testing/realtimeStatus/image/icon/offline-01.svg";
            break;
        case enumOnlineStatus[1]:
            imgName = "../../../../static/Testing/realtimeStatus/image/icon/online-01.svg";
            break;
        case enumOnlineStatus[2]:
            imgName = "../../../../static/Testing/realtimeStatus/image/icon/running-01.svg";
            break;
        case enumOnlineStatus[3]:
            imgName = "../../../../static/Testing/realtimeStatus/image/icon/alarm-01.svg";
            break;
        default:
            break;
    }
    var devIcon = new BMap.Icon(imgName, new BMap.Size(devIconWidth, devIconHeight));
    var devMarker = new BMap.Marker(markPoint, { icon: devIcon });      //创建Mark
    devMarker.addEventListener('click', function() {
        onSelectDevice(baseInfo.deviceId);
    })

    setMyBaseWin(thisMap, baseInfo, devMarker);

    var devLabel = new BMap.Label(baseInfo.deviceId);                            //以仪器id为Label，但不显示label
    devMarker.setLabel(devLabel);
    devLabel.hide();

    //alert(devMarker.getLabel().content);

    devMarks.push(devMarker);                                   //加入聚合点

    return devMarker;
}

//创建基础信息窗
function setMyBaseWin(thisMap, baseInfo, devMarker){
    var devInfoStr = devBaseTemplate.StringFormat(baseInfo.deviceId,baseInfo.deviceAlias,baseInfo.deviceProUnitName,baseInfo.areaName,baseInfo.devicePurUnitName,baseInfo.onlineState,baseInfo.healthState);
    var devBaseWin = new BMap.InfoWindow(devInfoStr, devBaseWinStyle);
    devMarker.addEventListener("mouseover", function (e) { thisMap.openInfoWindow(devBaseWin,devMarker.getPosition()); });
    devMarker.addEventListener("mouseout", function (e) { thisMap.closeInfoWindow(devBaseWin,devMarker.getPosition()); });
}

//创建数据信息窗
function setMyMeasureWin(thisMap, measureInfo, devMarker){
    var measureStr = measureTemplate.StringFormat(measureInfo.batchNumber,measureInfo.measureTime,measureInfo.resultO2Avg,measureInfo.resultCOAvg,
        measureInfo.resultCO2Avg,measureInfo.resultSO2Avg,measureInfo.resultNOAvg,measureInfo.resultNO2Avg,measureInfo.resultNOXAvg,measureInfo.resultH2SAvg);
    var measureWin = new BMap.InfoWindow(measureStr, measureWinStyle);
    devMarker.addEventListener("rightclick", function (e) { thisMap.openInfoWindow(measureWin,devMarker.getPosition()); });
}
