﻿//格式化字符串
String.prototype.StringFormat = function () {
    if (arguments.length == 0) {
        return this;
    }
    for (var StringFormat_s = this, StringFormat_i = 0; StringFormat_i < arguments.length; StringFormat_i++) {
        StringFormat_s = StringFormat_s.replace(new RegExp("\\{" + StringFormat_i + "\\}", "g"), arguments[StringFormat_i]);
    }
    return StringFormat_s;
};

var template1 = '<div style="margin:0;line-height:20px;padding:2px;">' +
                '<img src="../../../static/CustomerService/image/{3}" '+ 
                'alt="" style="float:right;zoom:1;overflow:hidden;width:130px;height:160px;margin-left:3px;"/>' +
                '<p>名称：{0}<br/>' + '{1}'
                +'<br/>地址：{2}</p>' +
                '</div>';

function addMarkObjInfo(servInfo) {		
    var tmptxt = template1.StringFormat(servInfo.name, servInfo.contactInfo.replace(/;/g , '</br>'), 
    		servInfo.address, servInfo.photoName);
    //alert(tmptxt);
    var service_point = new BMap.Point(servInfo.longitude, servInfo.latitude);					//取得标注坐标
    var marker = new BMap.Marker(service_point);	//创建marker对象,{icon:mcIcon}
    var label = new BMap.Label(servInfo.name, { offset: new BMap.Size(20, -10) });
    marker.setLabel(label);
    //marker.setAnimation(BMAP_ANIMATION_BOUNCE); //跳动的动画
    map.addOverlay(marker);																															//在地图中添加marker	
    																													
    var searchInfoWindow = new BMapLib.SearchInfoWindow(map, tmptxt, winStyle);			//创建带检索功能的信息窗	    
    marker.addEventListener("click", function (e) { searchInfoWindow.open(marker); });	//添加事件监听函数marker.addEventListener("click", function (e) { searchInfoWindow.open(marker); });	//添加事件监听函数
}
